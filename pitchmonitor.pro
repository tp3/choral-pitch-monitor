#
# To compile Android on Mint/Ubuntu install gcc-arm-linux-androidabi
#
# QT-SPECIFIC ISSUES
#
# To enable fast compilation in Qt insert the following environment variable:
# Tools > Options > Build & Run > Kits > (pick your toolchain) -> Environment-> Change...
# MAKEFLAGS=-j8
#
#
# PLATFORM-SPECIFIC ISSUES
#
# MACOSX/IOS:
# In case of a  "sysroot not found" error (wrong XCode library) the reason might be
# a hidden .qmake.stash or -qmake.cache file in the build directory (use locate)
#
# WINDOWS UWP:
# Add additional Make-Install in the project settings of Qt.
# The reason is that fftw3.dll needs to be copied.
#
TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS += \
    thirdparty \
    application \

application.depends = thirdparty
