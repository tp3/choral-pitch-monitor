### Choral Pitch Monitor

Haye Hinrichsen <br>
Institute for Physics and Astronomy <br>
Department of Theoretical Physics III<br>
University of Würzburg<br> D-97074 Würzburg, Germany

## Building

To build the application please download the community version of [Qt](https://www.qt.io/download) and open the project file pitchmonitor.pro with QtCreator.

## Direct download

You can [download the application](https://pitchmonitor.tp3.app/download) directly
from our [website](https://pitchmonitor.tp3.app).

## License

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

## Included third party software components
* [Qt (Community version)](https://www.qt.io/download)
* [FFTW3](http://www.fftw.org)
