/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>

#include "config.h"
#include "application.h"
#include "translator.h"
#include "appstatehandler.h"
#include "platformtools/platformtools.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Main function of the application
/// \param argc : Argument count (not used)
/// \param argv : Argument vector (not used)
/// \return error code
///////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    int errorcode = 0;

    // Create GUI
    QGuiApplication app(argc, argv);

    // Specify unique identifiers, needed for QSettings
    app.setOrganizationName(INT_ORGANIZATIONNAME);
    app.setOrganizationDomain(INT_ORGANIZATIONDOMAIN);
    app.setApplicationName(INT_APPLICATIONNAME);

    // Load the application icon
    app.setWindowIcon(QIcon(":/icons/tuningfork.png"));

    // Register the translator
    qmlRegisterType<Translator> ("PitchMonitor",1,0,"Translator");

    // Create QML engine
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty()) return -1;

    // Depending on platform, suppress the screensaver
    PlatformTools::getSingleton().keepScreenOn(true);

    // Depending on platform, ask for permission to use the microphone
    PlatformTools::getSingleton().requestPermissions(PlatformPermission::microphone);

    // Create applicaiton
    Application application(app);

    // Initialize translator
    Translator::init(app,engine,INT_APPLICATIONNAME_LOWERCASE,":/languages/translations");

    // Initialize and start the application
    application.init(engine);
    application.start();

    // Execute the application
    errorcode = app.exec();

    // Shut down
    application.stop();
    application.exit();

    return errorcode;
}
