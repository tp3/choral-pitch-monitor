/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include <QtGlobal>

//=============================================================================
//              Compile-time configuration of the application
//=============================================================================

#ifndef CONFIG_H
#define CONFIG_H

//------------------------- Application name etc ------------------------------

#define INT_APPLICATIONNAME "PitchMonitor"
#define INT_APPLICATIONNAME_LOWERCASE "pitchmonitor"
#define INT_ORGANIZATIONNAME "University of Wuerzburg"
#define INT_ORGANIZATIONDOMAIN "tp3.app"

//----------------------------- Version numbers -------------------------------

// To modify the version number update here and then call script upateVersion.h
#define INT_APPLICATION_VERSION "1.0"
#define INT_ROLLING_APPLICATION_VERSION 7


#endif // CONFIG_H
