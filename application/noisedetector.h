/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef NOISEDETECTOR_H
#define NOISEDETECTOR_H

#include <QObject>
#include <QVector>
#include <QTimer>

#include <QFile>
#include <QTextStream>
#include <QString>

#include "audioinput.h"
#include "fouriertransform.h"

//=============================================================================
//                             Class NoiseDetector
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \class NoiseDetector
/// \brief The purpose of this module is to distinguish speech/noise
/// and music. This idea is that the pitch monitor should be disabled while
/// the choir is not singing and no instrument is playing.
///////////////////////////////////////////////////////////////////////////////

class NoiseDetector : public QObject
{
    Q_OBJECT

    using complex = std::complex<double>;

public:
    NoiseDetector(AudioInput &audioInput);

signals:
    void signalEnable (QVariant on);       ///< Activate/deactivate circular gauge

public slots:
    void start();
    void stop();
    void setNoiseDetectorParameters (bool noiseDetectionEnabled, double noiseThreshold);

private slots:
    void detect();

private:
    void init();
    enum state { off, on, undefined };
    void setOperationMode (state s, QString msg);

private:
    QTimer* pDetectorTimer;                 ///< Timer that triggers the noise detector periodically
    AudioInput &rAudioInput;                ///< Pointer to the audio input module
    int mSampleRate, mAudioBufferSize;           ///< Audio parameters

    FourierTransform mFFT;                  ///< Fast Fourier transform module
    QVector<double> mSample;                ///< The actual audio sample to be analyzed
    QVector<complex> mSpectrum;             ///< The spectrum of the sample produced by the FFT

    int mKmin, mKmax;                       ///< Indices of Fourier modes to be evaluated
    QVector<double> mCyclicSpectrum;        ///< Cyclically mapped logarithmic spectrum
    QVector<double> mOctaveSpectrum;        ///< Octave-oriented logarithmic spectrum
    QVector<double> mPreviousSpectrum;      ///< Previous octave spectrum
    QVector<int> mCyclicLogMap;             ///< Cyclic logarithmic map
    QVector<int> mOctaveLogMap;             ///< Octave logarithmic map

    bool mInitialized;                      ///< Flag indicating proper initialization
    double mNoiseThreshold;                 ///< Actual noise threshold
    double mPreviousTotalPower;             ///< Measured total power of the previous update
};

#endif // NOISEDETECTOR_H
