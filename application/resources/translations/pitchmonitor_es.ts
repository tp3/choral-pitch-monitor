<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AudioInput</name>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Audio device not found.</source>
        <translation>Dispositivo de audio no encontrado.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Using default device instead.</source>
        <translation>Usando el dispositivo de audio estándar.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="156"/>
        <source>Cannot find audio input device.</source>
        <translation>No se puede encontrar el dispositivo de entrada de audio.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="166"/>
        <source>Cannot open audio device.</source>
        <translation>Impossible d&apos;ouvrir le périphérique audio.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="445"/>
        <source>Automatic selection</source>
        <translation>Selección automática</translation>
    </message>
</context>
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../qml/ControlBar.qml" line="49"/>
        <source>Information and Help</source>
        <translation>Información y Ayuda</translation>
    </message>
    <message>
        <location filename="../qml/ControlBar.qml" line="60"/>
        <source>Enter Settings</source>
        <translation>Entrar en la configuración</translation>
    </message>
</context>
<context>
    <name>HelpView</name>
    <message>
        <location filename="../qml/HelpView.qml" line="35"/>
        <source>About this App</source>
        <translation>Acerca de esta aplicación</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="122"/>
        <source>University of Würzburg, Germany</source>
        <translation>Universidad de Würzburg, Alemania</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="123"/>
        <source>Free software based on</source>
        <translation>Software gratuito basado en</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="125"/>
        <source>and</source>
        <translation>y</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="127"/>
        <source>licensed under</source>
        <translation>licenciado bajo</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="128"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="143"/>
        <source>Usage:</source>
        <translation>Uso:</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="144"/>
        <source>Choirs often suffer from the problem of a collective pitch drift.</source>
        <translation>Para muchos coros, la frecuencia de tono promedio disminuye durante el canto.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="145"/>
        <source>This app allows you to monitor the average pitch deviation of all voices while singing.</source>
        <translation>Esta aplicación le permite monitorear la desviación de frecuencia promedio de todas las voces mientras canta.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="146"/>
        <source>Warning signs will appear if the pitch deviation becomes too large.</source>
        <translation>Aparecerán señales de advertencia si la desviación del tono es demasiado grande.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="147"/>
        <source>To define the reference pitch simply tap the tuning fork and play a few notes.</source>
        <translation>Para definir el tono de referencia, simplemente toque el diapasón de afinación y toque algunas notas.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="148"/>
        <source>More information at</source>
        <translation>Mas información en</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="60"/>
        <source>A cent equals 1/100 of a semitone</source>
        <translation>Un cent es igual a 1/100 parte de un semitono</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="86"/>
        <source>Click at the axis to reset gauge</source>
        <translation>Haga clic en el eje para restablecer el indicador</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="113"/>
        <source>Tap Tuning Fork to define Reference Frequency</source>
        <translation>Toque el diapasón para definir la frecuencia de referencia</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="139"/>
        <source>Reference frequency</source>
        <translation>Frecuencia de referencia</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../qml/SettingsView.qml" line="46"/>
        <source>Settings</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="108"/>
        <source>Application Title:</source>
        <translation>Titulo de la aplicación:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="120"/>
        <source>Choose your favorite App title</source>
        <translation>Elija su título favorito de la aplicación aquí</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="135"/>
        <source>Language:</source>
        <translation>Idioma:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="162"/>
        <source>Audio input device:</source>
        <translation>Dispositivo de audio:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="223"/>
        <source>Reference frequency:</source>
        <translation>Frecuencia de ref.:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="256"/>
        <source>Warning threshold:</source>
        <translation>Umbral de advertencia:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="288"/>
        <source>Noise detection:</source>
        <translation>Detección de ruido:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="326"/>
        <source>Reaction speed:</source>
        <translation>Velocidad de reacción:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="353"/>
        <source>Loudness indicator:</source>
        <translation>Indicador de volumen:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="392"/>
        <source>Reset to default values</source>
        <translation>Restablecer a los valores predeterminados</translation>
    </message>
</context>
<context>
    <name>Squelch</name>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Noise detector</source>
        <translation>Detector de ruido</translation>
    </message>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Volume indicator</source>
        <translation>Indicador de volumen</translation>
    </message>
</context>
<context>
    <name>TopLine</name>
    <message>
        <location filename="../qml/TopLine.qml" line="59"/>
        <location filename="../qml/TopLine.qml" line="71"/>
        <source>Go back</source>
        <translation>Retorno</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>Automático</translation>
    </message>
</context>
<context>
    <name>TuningMessage</name>
    <message>
        <location filename="../qml/TuningMessage.qml" line="47"/>
        <source>Please play a few notes...</source>
        <translation>Por favor toca algunas notas ...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="124"/>
        <source>Measuring reference pitch...</source>
        <translation>Medición de frecuencia de referencia...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="139"/>
        <source>New reference pitch</source>
        <translation>Nueva frecuencia de referencia</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="180"/>
        <source>Calibration</source>
        <translation>Calibración</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="33"/>
        <source>Choral Pitch Monitor</source>
        <translation>Monitor de tono coral</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>searching...</source>
        <translation>buscando...</translation>
    </message>
</context>
</TS>
