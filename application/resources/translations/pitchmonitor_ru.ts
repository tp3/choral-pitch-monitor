<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AudioInput</name>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Audio device not found.</source>
        <translation>Аудиоустройство не найдено.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Using default device instead.</source>
        <translation>Вместо этого используйте устройство по умолчанию.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="156"/>
        <source>Cannot find audio input device.</source>
        <translation>Не удается найти устройство ввода аудио.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="166"/>
        <source>Cannot open audio device.</source>
        <translation>Не удается открыть аудиоустройство.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="445"/>
        <source>Automatic selection</source>
        <translation>Автоматический выбор</translation>
    </message>
</context>
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../qml/ControlBar.qml" line="49"/>
        <source>Information and Help</source>
        <translation>Информация и помощь</translation>
    </message>
    <message>
        <location filename="../qml/ControlBar.qml" line="60"/>
        <source>Enter Settings</source>
        <translation>Введите настройки</translation>
    </message>
</context>
<context>
    <name>HelpView</name>
    <message>
        <location filename="../qml/HelpView.qml" line="35"/>
        <source>About this App</source>
        <translation>Об этом приложении</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="122"/>
        <source>University of Würzburg, Germany</source>
        <translation>Университет Вюрцбурга, Германия</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="123"/>
        <source>Free software based on</source>
        <translation>Бесплатное программное обеспечение на основе</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="125"/>
        <source>and</source>
        <translation>и</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="127"/>
        <source>licensed under</source>
        <translation>опубликованный по лицензии</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="128"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="143"/>
        <source>Usage:</source>
        <translation>Инструкции:</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="144"/>
        <source>Choirs often suffer from the problem of a collective pitch drift.</source>
        <translation>Многие хоры, как правило, медленно понижают шаг во время пения.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="145"/>
        <source>This app allows you to monitor the average pitch deviation of all voices while singing.</source>
        <translation>Это приложение позволяет отслеживать среднее отклонение частоты всех голосов во время пения.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="146"/>
        <source>Warning signs will appear if the pitch deviation becomes too large.</source>
        <translation>Предупреждающие знаки появятся, если отклонение высоты тона станет слишком большим.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="147"/>
        <source>To define the reference pitch simply tap the tuning fork and play a few notes.</source>
        <translation>Чтобы установить опорную частоту, просто коснитесь вилки настройки, а затем воспроизведите или пойте несколько заметок.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="148"/>
        <source>More information at</source>
        <translation>Дополнительная информация на</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="60"/>
        <source>A cent equals 1/100 of a semitone</source>
        <translation>Один цент равен 1/100 части полутона</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="86"/>
        <source>Click at the axis to reset gauge</source>
        <translation>Нажмите на ось, чтобы сбросить индикатор</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="113"/>
        <source>Tap Tuning Fork to define Reference Frequency</source>
        <translation>Нажмите камертон, чтобы установить опорную частоту</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="139"/>
        <source>Reference frequency</source>
        <translation>Опорная частота</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../qml/SettingsView.qml" line="46"/>
        <source>Settings</source>
        <translation>регулировка</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="108"/>
        <source>Application Title:</source>
        <translation>Название приложения:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="120"/>
        <source>Choose your favorite App title</source>
        <translation>Вставьте свой собственный заголовок приложения здесь</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="135"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="162"/>
        <source>Audio input device:</source>
        <translation>Устройство ввода аудио:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="223"/>
        <source>Reference frequency:</source>
        <translation>Опорная частота:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="256"/>
        <source>Warning threshold:</source>
        <translation>Предупреждающий порог:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="288"/>
        <source>Noise detection:</source>
        <translation>Обнаружение шума:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="326"/>
        <source>Reaction speed:</source>
        <translation>скорость реакции:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="353"/>
        <source>Loudness indicator:</source>
        <translation>Индикатор громкости:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="392"/>
        <source>Reset to default values</source>
        <translation>Сбросить настройки по умолчанию</translation>
    </message>
</context>
<context>
    <name>Squelch</name>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Noise detector</source>
        <translation>Датчик шума</translation>
    </message>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Volume indicator</source>
        <translation>Индикатор громкости</translation>
    </message>
</context>
<context>
    <name>TopLine</name>
    <message>
        <location filename="../qml/TopLine.qml" line="59"/>
        <location filename="../qml/TopLine.qml" line="71"/>
        <source>Go back</source>
        <translation>вернуть</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>автоматическая</translation>
    </message>
</context>
<context>
    <name>TuningMessage</name>
    <message>
        <location filename="../qml/TuningMessage.qml" line="47"/>
        <source>Please play a few notes...</source>
        <translation>Пожалуйста, сыграйте несколько заметок ...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="124"/>
        <source>Measuring reference pitch...</source>
        <translation>Измерение опорного тона ...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="139"/>
        <source>New reference pitch</source>
        <translation>Новый опорный шаг</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="180"/>
        <source>Calibration</source>
        <translation>калибровка</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="33"/>
        <source>Choral Pitch Monitor</source>
        <translation>Монитор хоровой высоты</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>searching...</source>
        <translation>поиск ...</translation>
    </message>
</context>
</TS>
