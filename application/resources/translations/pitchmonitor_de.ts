<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AudioInput</name>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Audio device not found.</source>
        <translation>Audiogerät nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Using default device instead.</source>
        <translation>Verwende stattdessen das Standard-Audiogerät.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="156"/>
        <source>Cannot find audio input device.</source>
        <translation>Audioquelle nicht auffindbar.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="166"/>
        <source>Cannot open audio device.</source>
        <translation>Audiogerät kann nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="445"/>
        <source>Automatic selection</source>
        <translation>Automatische Auswahl</translation>
    </message>
</context>
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../qml/ControlBar.qml" line="49"/>
        <source>Information and Help</source>
        <translation>Information und Hilfe</translation>
    </message>
    <message>
        <location filename="../qml/ControlBar.qml" line="60"/>
        <source>Enter Settings</source>
        <translation>Einstellungsdialog aufrufen</translation>
    </message>
</context>
<context>
    <name>HelpView</name>
    <message>
        <location filename="../qml/HelpView.qml" line="35"/>
        <source>About this App</source>
        <translation>Über diese Anwendung</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="122"/>
        <source>University of Würzburg, Germany</source>
        <translation>Universität Würzburg</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="123"/>
        <source>Free software based on</source>
        <translation>Freie Software basierend auf</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="125"/>
        <source>and</source>
        <translation>und</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="127"/>
        <source>licensed under</source>
        <translation>lizensiert unter</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="128"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="143"/>
        <source>Usage:</source>
        <translation>Kurzanleitung:</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="144"/>
        <source>Choirs often suffer from the problem of a collective pitch drift.</source>
        <translation type="unfinished">Viele Chöre neigen dazu, zu tief zu singen, so dass die mittlere Tonhöhe während des Singens langsam absinkt.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="145"/>
        <source>This app allows you to monitor the average pitch deviation of all voices while singing.</source>
        <translation>Mit dieser App können Sie die durchschnittliche Tonhöhenabweichung während des Singens beobachten.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="146"/>
        <source>Warning signs will appear if the pitch deviation becomes too large.</source>
        <translation>Sobald die Tonhöhenabweichung zu groß wird erscheinen Warnhinweise.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="147"/>
        <source>To define the reference pitch simply tap the tuning fork and play a few notes.</source>
        <translation>Um die Bezugstonhöhe festzulegen, tippen Sie einfach auf die Stimmgabel und spielen ein paar Töne.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="148"/>
        <source>More information at</source>
        <translation>Weitere Informationen unter</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="60"/>
        <source>A cent equals 1/100 of a semitone</source>
        <translation>Ein Cent entspricht dem 1/100 Teil eines Halbtons</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="86"/>
        <source>Click at the axis to reset gauge</source>
        <translation>Klicken Sie auf die Achse, um die Anzeige zurückzusetzen</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="113"/>
        <source>Tap Tuning Fork to define Reference Frequency</source>
        <translation>Tippen Sie auf die Stimmgabel, um die Referenzfrequenz einzustellen</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="139"/>
        <source>Reference frequency</source>
        <translation>Bezugsfrequenz</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../qml/SettingsView.qml" line="46"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="108"/>
        <source>Application Title:</source>
        <translation>Titel der Anwendung:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="120"/>
        <source>Choose your favorite App title</source>
        <translation>Fügen Sie hier Ihren eigenen App-Titel ein</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="135"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="162"/>
        <source>Audio input device:</source>
        <translation>Audioquelle:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="223"/>
        <source>Reference frequency:</source>
        <translation>Bezugsfrequenz:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="256"/>
        <source>Warning threshold:</source>
        <translation>Warnschwelle:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="288"/>
        <source>Noise detection:</source>
        <translation>Geräuscherkennung:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="326"/>
        <source>Reaction speed:</source>
        <translation>Reaktionsgeschwindigkeit:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="353"/>
        <source>Loudness indicator:</source>
        <translation>Lautstärke-Indikator:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="392"/>
        <source>Reset to default values</source>
        <translation>Auf Standardwerte zurücksetzen</translation>
    </message>
</context>
<context>
    <name>Squelch</name>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Noise detector</source>
        <translation>Störgeräuschdetektor</translation>
    </message>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Volume indicator</source>
        <translation>Lautstärke-Indikator</translation>
    </message>
</context>
<context>
    <name>TopLine</name>
    <message>
        <location filename="../qml/TopLine.qml" line="59"/>
        <location filename="../qml/TopLine.qml" line="71"/>
        <source>Go back</source>
        <translation>Gehe zurück</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
</context>
<context>
    <name>TuningMessage</name>
    <message>
        <location filename="../qml/TuningMessage.qml" line="47"/>
        <source>Please play a few notes...</source>
        <translation>Bitte spielen Sie einige Töne...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="124"/>
        <source>Measuring reference pitch...</source>
        <translation>Messung der Referenztonhöhe...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="139"/>
        <source>New reference pitch</source>
        <translation>Neue Referenztonhöhe</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="180"/>
        <source>Calibration</source>
        <translation>Kalibrierung</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="33"/>
        <source>Choral Pitch Monitor</source>
        <translation>Tonhöhenmonitor für Chöre</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>searching...</source>
        <translation>suche...</translation>
    </message>
</context>
</TS>
