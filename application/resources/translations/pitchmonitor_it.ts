<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>AudioInput</name>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Audio device not found.</source>
        <translation>Dispositivo audio non trovato.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Using default device instead.</source>
        <translation>Utilizzare invece il dispositivo predefinito.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="156"/>
        <source>Cannot find audio input device.</source>
        <translation>Impossibile trovare il dispositivo di input audio.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="166"/>
        <source>Cannot open audio device.</source>
        <translation>Il dispositivo audio non può essere aperto.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="445"/>
        <source>Automatic selection</source>
        <translation>Selezione automatica</translation>
    </message>
</context>
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../qml/ControlBar.qml" line="49"/>
        <source>Information and Help</source>
        <translation>Informazioni e aiuto</translation>
    </message>
    <message>
        <location filename="../qml/ControlBar.qml" line="60"/>
        <source>Enter Settings</source>
        <translation>Inserisci le preferenze</translation>
    </message>
</context>
<context>
    <name>HelpView</name>
    <message>
        <location filename="../qml/HelpView.qml" line="35"/>
        <source>About this App</source>
        <translation>Informazioni su questa app</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="122"/>
        <source>University of Würzburg, Germany</source>
        <translation>Università di Würzburg, Germania</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="123"/>
        <source>Free software based on</source>
        <translation>Software gratuito basato su</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="125"/>
        <source>and</source>
        <translation>e</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="127"/>
        <source>licensed under</source>
        <translation>concesso in licenza sotto</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="128"/>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="143"/>
        <source>Usage:</source>
        <translation>Uso:</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="144"/>
        <source>Choirs often suffer from the problem of a collective pitch drift.</source>
        <translation>Per molti cori, la frequenza media dei toni diminuisce durante il canto.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="145"/>
        <source>This app allows you to monitor the average pitch deviation of all voices while singing.</source>
        <translation>Questa app ti permette di monitorare la deviazione di frequenza media di tutte le voci mentre canti.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="146"/>
        <source>Warning signs will appear if the pitch deviation becomes too large.</source>
        <translation>Appaiono i segnali di avvertimento se la deviazione del beccheggio diventa troppo grande.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="147"/>
        <source>To define the reference pitch simply tap the tuning fork and play a few notes.</source>
        <translation>Per definire il tono di riferimento, basta toccare il diapason e suonare alcune note.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="148"/>
        <source>More information at</source>
        <translation>Maggiori informazioni a</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="60"/>
        <source>A cent equals 1/100 of a semitone</source>
        <translation>Un centesimo è uguale alla parte 1/100 di un semitono</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="86"/>
        <source>Click at the axis to reset gauge</source>
        <translation>Clicca sull&apos;asse per resettare l&apos;indicatore</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="113"/>
        <source>Tap Tuning Fork to define Reference Frequency</source>
        <translation>Tocca Diapason per definire la frequenza di riferimento</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="139"/>
        <source>Reference frequency</source>
        <translation>Frequenza di riferimento</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../qml/SettingsView.qml" line="46"/>
        <source>Settings</source>
        <translation>Preferenze</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="108"/>
        <source>Application Title:</source>
        <translation>Titolo dell&apos;applicazione:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="120"/>
        <source>Choose your favorite App title</source>
        <translation>Scegli il tuo titolo dell&apos;app preferito</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="135"/>
        <source>Language:</source>
        <translation>Linguaggio:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="162"/>
        <source>Audio input device:</source>
        <translation>Dispositivo di input audio:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="223"/>
        <source>Reference frequency:</source>
        <translation>Frequenza di rif.:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="256"/>
        <source>Warning threshold:</source>
        <translation>Soglia di avviso:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="288"/>
        <source>Noise detection:</source>
        <translation>Rilevazione del rumore:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="326"/>
        <source>Reaction speed:</source>
        <translation>Velocità di reazione:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="353"/>
        <source>Loudness indicator:</source>
        <translation>Indicatore del volume:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="392"/>
        <source>Reset to default values</source>
        <translation>Ripristina valori predefiniti</translation>
    </message>
</context>
<context>
    <name>Squelch</name>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Noise detector</source>
        <translation>Rilevatore di rumore</translation>
    </message>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Volume indicator</source>
        <translation>Indicatore del volume</translation>
    </message>
</context>
<context>
    <name>TopLine</name>
    <message>
        <location filename="../qml/TopLine.qml" line="59"/>
        <location filename="../qml/TopLine.qml" line="71"/>
        <source>Go back</source>
        <translation>Ritorno</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>Automatico</translation>
    </message>
</context>
<context>
    <name>TuningMessage</name>
    <message>
        <location filename="../qml/TuningMessage.qml" line="47"/>
        <source>Please play a few notes...</source>
        <translation>Si prega di suonare qualche nota ...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="124"/>
        <source>Measuring reference pitch...</source>
        <translation>Frequenza di riferimento di misurazione...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="139"/>
        <source>New reference pitch</source>
        <translation>Nuova frequenza di riferimento</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="180"/>
        <source>Calibration</source>
        <translation>Calibrazione</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="33"/>
        <source>Choral Pitch Monitor</source>
        <translation>Monitor del tono corale</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>searching...</source>
        <translation>ricerca...</translation>
    </message>
</context>
</TS>
