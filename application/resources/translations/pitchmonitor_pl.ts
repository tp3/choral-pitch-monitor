<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AudioInput</name>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Audio device not found.</source>
        <translation>Nie znaleziono urządzenia audio.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="144"/>
        <source>Using default device instead.</source>
        <translation>Zamiast tego używane jest urządzenie domyślne.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="156"/>
        <source>Cannot find audio input device.</source>
        <translation>Nie można znaleźć urządzenia wejściowego audio.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="166"/>
        <source>Cannot open audio device.</source>
        <translation>Nie można otworzyć urządzenia audio.</translation>
    </message>
    <message>
        <location filename="../../modules/audio/audioinput.cpp" line="445"/>
        <source>Automatic selection</source>
        <translation>Automatyczny wybór</translation>
    </message>
</context>
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../qml/ControlBar.qml" line="49"/>
        <source>Information and Help</source>
        <translation>Informacje i pomoc</translation>
    </message>
    <message>
        <location filename="../qml/ControlBar.qml" line="60"/>
        <source>Enter Settings</source>
        <translation>Wejdź w Ustawienia</translation>
    </message>
</context>
<context>
    <name>HelpView</name>
    <message>
        <location filename="../qml/HelpView.qml" line="35"/>
        <source>About this App</source>
        <translation>O tej aplikacji</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="122"/>
        <source>University of Würzburg, Germany</source>
        <translation>Uniwersytet w Würzburgu, Niemcy</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="123"/>
        <source>Free software based on</source>
        <translation>Darmowe oprogramowanie oparte na</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="125"/>
        <source>and</source>
        <translation>i</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="127"/>
        <source>licensed under</source>
        <translation>licencjonowane na mocy</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="128"/>
        <source>Version</source>
        <translation>wersja</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="143"/>
        <source>Usage:</source>
        <translation>Stosowanie:</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="144"/>
        <source>Choirs often suffer from the problem of a collective pitch drift.</source>
        <translation>Śpiewacy w chórach często nie zachowują swojej intonacji.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="145"/>
        <source>This app allows you to monitor the average pitch deviation of all voices while singing.</source>
        <translation>Ta aplikacja pozwala monitorować średnie odchylenie wysokości wszystkich głosów podczas śpiewania.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="146"/>
        <source>Warning signs will appear if the pitch deviation becomes too large.</source>
        <translation>Znaki ostrzegawcze pojawią się, jeśli odchylenie wysokości tonu staje się zbyt duże.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="147"/>
        <source>To define the reference pitch simply tap the tuning fork and play a few notes.</source>
        <translation>Aby zdefiniować wysokość odniesienia, po prostu stuknij w kamerton i zagraj kilka nut.</translation>
    </message>
    <message>
        <location filename="../qml/HelpView.qml" line="148"/>
        <source>More information at</source>
        <translation>Więcej informacji na</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="60"/>
        <source>A cent equals 1/100 of a semitone</source>
        <translation>Cent oznacza 1/100 półtonu</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="86"/>
        <source>Click at the axis to reset gauge</source>
        <translation>Kliknij na osi, aby zresetować wskaźnik</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="113"/>
        <source>Tap Tuning Fork to define Reference Frequency</source>
        <translation>Dotknij kamertonu, aby ustawić częstotliwość odniesienia</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="139"/>
        <source>Reference frequency</source>
        <translation>Częstotliwość odniesienia</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../qml/SettingsView.qml" line="46"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="108"/>
        <source>Application Title:</source>
        <translation>Tytuł aplikacji:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="120"/>
        <source>Choose your favorite App title</source>
        <translation>Wybierz swój ulubiony tytuł aplikacji</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="135"/>
        <source>Language:</source>
        <translation>Język:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="162"/>
        <source>Audio input device:</source>
        <translation>Urządzenie wejściowe audio:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="223"/>
        <source>Reference frequency:</source>
        <translation>Częstotliwość bazowa:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="256"/>
        <source>Warning threshold:</source>
        <translation>Próg ostrzegawczy:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="288"/>
        <source>Noise detection:</source>
        <translation>Wykrywanie hałasu:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="326"/>
        <source>Reaction speed:</source>
        <translation>Prędkość reakcji:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="353"/>
        <source>Loudness indicator:</source>
        <translation>Wskaźnik głośności:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="392"/>
        <source>Reset to default values</source>
        <translation>Zresetuj do wartości domyślnych</translation>
    </message>
</context>
<context>
    <name>Squelch</name>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Volume indicator</source>
        <translation>Wskaźnik głośności</translation>
    </message>
    <message>
        <location filename="../qml/Squelch.qml" line="41"/>
        <source>Noise detector</source>
        <translation>Detektor hałasu</translation>
    </message>
</context>
<context>
    <name>TopLine</name>
    <message>
        <location filename="../qml/TopLine.qml" line="59"/>
        <location filename="../qml/TopLine.qml" line="71"/>
        <source>Go back</source>
        <translation>Wróć</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>Automatyczny</translation>
    </message>
</context>
<context>
    <name>TuningMessage</name>
    <message>
        <location filename="../qml/TuningMessage.qml" line="47"/>
        <source>Please play a few notes...</source>
        <translation>Proszę zagrać kilka nut...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="124"/>
        <source>Measuring reference pitch...</source>
        <translation>Pomiar częstotliwości odniesienia...</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="139"/>
        <source>New reference pitch</source>
        <translation>Nowa częstotliwość odniesienia</translation>
    </message>
    <message>
        <location filename="../qml/TuningMessage.qml" line="180"/>
        <source>Calibration</source>
        <translation>Kalibrowanie</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="33"/>
        <source>Choral Pitch Monitor</source>
        <translation>Monitor wysokości dla chórów</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>searching...</source>
        <translation>poszukiwania ...</translation>
    </message>
</context>
</TS>
