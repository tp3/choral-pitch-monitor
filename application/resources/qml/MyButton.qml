/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0

Item {
    property string source: ""
    property string tooltip: "tooltip undefined"
    property bool tooltipEnabled: true
    property double imageOpacity: 1
    property double margins: 0
    signal clicked()

    width: mainwindow.width*0.1
    height: mainwindow.width*0.1

//    Rectangle  {
//        anchors.fill: parent
//        color: "red"
//    }

    Image {
        id: img
        anchors.fill: parent
        anchors.margins: parent.margins
        source: parent.source
        opacity: (buttonArea.pressed || buttonArea.hovered ? 1 : imageOpacity)
        scale: (buttonArea.pressed || buttonArea.hovered ? 1.1 : 0.9)
        fillMode: Image.PreserveAspectFit
        smooth: true
        mipmap: true
    }

    MyToolTip {
        text: tooltip
        visible: tooltipEnabled && (buttonArea.pressed || buttonArea.hovered)
    }

    Button {
        id: buttonArea
        opacity: 0
        anchors.fill: parent
        onClicked: { parent.clicked(); }
    }

}

