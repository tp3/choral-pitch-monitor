import QtQuick 2.12
import QtQuick.Controls 2.5

Switch {
    id: swmyitch
    indicator: Rectangle {
        implicitWidth: swmyitch.height*2
        implicitHeight: swmyitch.height
        x: swmyitch.leftPadding
        y: parent.height / 2 - height / 2
        radius: swmyitch.height/2
        color: swmyitch.checked ? "#333333" : "#cccccc"
        border.color: "#cccccc"
        border.width: height*0.05

        Rectangle {
            x: swmyitch.checked ? parent.width - width : 0
            width: swmyitch.height
            height: swmyitch.height
            radius: swmyitch.height/2
            color: swmyitch.down ? "#cccccc" : "#aaaaaa"
            border.color: "#ffffff"
            border.width: parent.border.width
        }
    }
}
