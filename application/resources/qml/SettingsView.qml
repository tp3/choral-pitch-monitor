/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10
import QtQuick.Controls 2.4

// Import this library for the rectangular glow
import QtGraphicalEffects 1.0

Item {
    property alias pitch: frequencySpinbox.value
    readonly property int maxPitch: 480
    readonly property int minPitch: 410
    readonly property double separation: 0.04
    function modifyPitch (difference) {
        if (difference > 0) {
            if (pitch<maxPitch) pitch++;
        }
        else {
            if (pitch>minPitch) pitch--;
        }
    }
    function flick(direction) {
        flickable.flick(0,direction*200);
    }


    TopLine {
        id: settingstitle
        headline: qsTr("Settings");
    }


    Item {
        id: item
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: settingstitle.bottom
        anchors.bottom: parent.bottom
        anchors.margins: parent.width*0.05

        RectangularGlow {
                id: effect
                opacity: 0.2
                anchors.fill: field
                glowRadius: field.radius
                spread: 0.5
                color: "white"
                cornerRadius: field.radius + glowRadius
            }

        Rectangle {
            id: field
            color: "black"
            opacity: 0.3
            anchors.fill:parent
            radius: parent.width*0.015;
        }

        // FLICKABLE SETTINGS REGION

        Flickable {
            id: flickable
            anchors.fill: field
            anchors.margins: parent.width*0.03
            contentHeight: parent.width*1.2
            contentWidth: parent.width*0.91
            flickableDirection: Flickable.VerticalFlick
            clip: true

            // SCROLLBAR

            ScrollBar.vertical: ScrollBar {
                width: parent.width * 0.035
                parent: flickable.parent
                anchors.top: flickable.top
                anchors.left: flickable.right
                anchors.bottom: flickable.bottom
                anchors.leftMargin: -parent.width*0.017
                policy: ScrollBar.AlwaysOn
            }


            // CUSTOM APP TITLE

            Text {
                id: appTitle
                color: "white"
                anchors.left: parent.left
                style: Text.Raised;
                styleColor: "black"
                text: qsTr("Application Title:");
                font.family: standardFont.name
                font.pixelSize: parent.width*0.05
            }

            MyTextField {
                id: apptextfield
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: appTitle.bottom
                anchors.topMargin: width*0.01
                height: parent.width*0.07
                placeholderText: qsTr("Choose your favorite App title");
                text: mainwindow.title
                onTextChanged: mainwindow.title=text
            }


            // APP LANGUAGE COMBOBOX

            Text {
                id: appLang
                color: "white"
                anchors.left: parent.left
                style: Text.Raised; styleColor: "black"
                anchors.top: apptextfield.bottom
                anchors.topMargin: parent.width*separation
                text: qsTr("Language:");
                font.family: standardFont.name
                font.pixelSize: appTitle.font.pixelSize
            }

            MyCombo {
                id: combolang
                fontsize: field.width*0.04
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: appLang.bottom
                anchors.topMargin: width*0.01
                height: apptextfield.height
                model: translator.languages
                currentIndex: translator.index
                onCurrentIndexChanged: if (count>0) translator.index=currentIndex
            }

            // AUDIO INPUT DEVICE COMBOBOX

            Text {
                id: input
                color: "white"
                anchors.left: parent.left
                anchors.top: combolang.bottom
                anchors.topMargin: parent.width*separation
                style: Text.Raised; styleColor: "black"
                text: qsTr("Audio input device:");
                font.family: standardFont.name
                font.pixelSize: appTitle.font.pixelSize
            }

            ProgressBar {
                id: vumeter
                anchors.left: input.right
                anchors.right: parent.right
                anchors.verticalCenter: input.verticalCenter
                anchors.leftMargin: parent.width*0.05
                height: input.height*0.35
                value: mainwindow.volume
                background: Rectangle {
                    implicitWidth: 200
                    implicitHeight: 6
                    color: "#ffffff"
                    radius: height*0.3
                }

                contentItem: Item {
                    implicitWidth: 200
                    implicitHeight: 4

                    Rectangle {
                        width: vumeter.visualPosition * parent.width
                        height: parent.height
                        radius: parent.height*0.3
                        color: "#202020"
                        border.color: "#c0c0c0"
                    }
                }
            }

            MyCombo {
                id: comboinput
                fontsize: field.width*(currentIndex==0 ? 0.04 : 0.033)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: input.bottom
                anchors.topMargin: width*0.01
                height: apptextfield.height
                model: mainwindow.deviceList
                property int deviceNumber: mainwindow.deviceNumber
                onDeviceNumberChanged: { currentIndex=deviceNumber; }
                currentIndex: mainwindow.deviceNumber
                onCurrentTextChanged: if (currentText !== "" && currentText !== inputDeviceName) {
                                            if (currentIndex===0) inputDeviceName="automatic";
                                            else inputDeviceName = currentText;
                                        }
            }

            // REFERENCE FREQUENCY

            Text {
                id: frequ
                color: "white"
                anchors.left: parent.left
                anchors.top: comboinput.bottom
                anchors.topMargin: parent.width*separation
                style: Text.Raised; styleColor: "black"
                text: qsTr("Reference frequency:");
                font.family: standardFont.name
                font.pixelSize: appTitle.font.pixelSize
            }

            MySpinBox {
                id: frequencySpinbox
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: parent.width*0.525
                anchors.top: frequ.bottom
                anchors.topMargin: width*0.01
                height: apptextfield.height
                from: 410
                to: 460
                stepSize: 1
                textFromValue: function(value, locale) {
                                        return ("%1 Hz").arg(value);
                               }
                property int pitch: mainwindow.referenceFrequency
                onPitchChanged: value=pitch
                onValueChanged: setReferenceFrequency(value)
            }

            // WARNING THRESHOLD

            Text {
                id: thresh
                color: "white"
                anchors.left: thresholdSpinbox.left
                anchors.top: comboinput.bottom
                anchors.topMargin: parent.width*separation
                style: Text.Raised; styleColor: "black"
                text: qsTr("Warning threshold:");
                font.family: standardFont.name
                font.pixelSize: appTitle.font.pixelSize
            }

            MySpinBox {
                id: thresholdSpinbox
                anchors.left: frequencySpinbox.right
                anchors.right: parent.right
                anchors.leftMargin: parent.width*0.05
                anchors.top: frequ.bottom
                anchors.topMargin: width*0.01
                height: apptextfield.height
                from: 15
                to: 50
                stepSize: 1
                textFromValue: function(value, locale) {
                                        return ("± %1 cent").arg(value);
                               }
                value: mainwindow.threshold
                onValueChanged: mainwindow.threshold=value
            }

            // SQUELCH

            Text {
                id: squelchText
                color: "white"
                anchors.left: parent.left
                anchors.top: thresholdSpinbox.bottom
                anchors.topMargin: parent.width*separation
                style: Text.Raised; styleColor: "black"
                text: qsTr("Noise detection:");
                font.family: standardFont.name
                font.pixelSize: appTitle.font.pixelSize
            }

            MySwitch {
                id: squelchSwitch
                anchors.left: parent.left
                anchors.top: squelchText.bottom
                anchors.topMargin: width*0.01+apptextfield.height*0.1
                height: apptextfield.height*0.9
                checked: mainwindow.noiseDetectionEnabled
                onCheckedChanged: mainwindow.noiseDetectionEnabled = checked;
            }

            MySlider {
                id: squelchSlider
                from: 0.05
                to: 1
                value: mainwindow.squelch
                onValueChanged: mainwindow.squelch=value
                visible: squelchSwitch.checked
                anchors.left: squelchSwitch.right
                anchors.right: parent.right
                anchors.top: squelchText.bottom
                anchors.topMargin: width*0.01
                height: apptextfield.height
            }

            // AGILITY

            Text {
                id: agilityText
                color: "white"
                anchors.left: parent.left
                anchors.top: squelchSwitch.bottom
                anchors.topMargin: parent.width*separation
                style: Text.Raised; styleColor: "black"
                text: qsTr("Reaction speed:");
                font.family: standardFont.name
                font.pixelSize: appTitle.font.pixelSize
            }

            MySlider {
                id: agilitySlider
                from: 0.05
                to: 1
                value: mainwindow.agility
                onValueChanged: mainwindow.agility=value
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: agilityText.bottom
                anchors.topMargin: width*0.01
                height: apptextfield.height
            }

            // LOUDNESS INDICATOR

            Text {
                id: loudnessText
                color: "white"
                anchors.left: parent.left
                anchors.top: agilitySlider.bottom
                anchors.topMargin: parent.width*separation
                style: Text.Raised; styleColor: "black"
                text: qsTr("Loudness indicator:");
                font.family: standardFont.name
                font.pixelSize: appTitle.font.pixelSize
            }


            MySwitch {
                id: dynamicsSwitch
                anchors.left: parent.left
                anchors.top: loudnessText.bottom
                anchors.topMargin: width*0.01+apptextfield.height*0.1
                height: apptextfield.height*0.9
                checked: mainwindow.dynamicalLevelEnabled
                onCheckedChanged: mainwindow.dynamicalLevelEnabled = checked;
            }

            Text {
                color: "white"
                anchors.left: dynamicsSwitch.right
                anchors.verticalCenter: dynamicsSwitch.verticalCenter
                anchors.verticalCenterOffset: -dynamicsSwitch.height*0.1
                style: Text.Raised; styleColor: "black"
                text: "   pp   p   mp   mf   f   ff";
                font.family: dynamicsFont.name
                font.pixelSize: appTitle.font.pixelSize
                opacity: dynamicsSwitch.checked ? 1 : 0.2
            }

            // RESET TO DEFAULT VALUES

            Button {
                id: control
                anchors.top: dynamicsSwitch.bottom
                anchors.topMargin: mainwindow.width*0.07
                width: mainwindow.width*0.7
                height: thresholdSpinbox.height*1.3
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: thresholdSpinbox.height*0.6
                font.family: standardFont.name
                text: qsTr("Reset to default values");
                onClicked: {
                    threshold = thresholdSpinbox.value = 30;
                    setReferenceFrequency(440);
                    comboinput.currentIndex=0;
                    mainwindow.title = apptextfield.text = "";
                    squelchSwitch.checked = true;
                    squelchSlider.value = defaultNoiseThreshold;
                    agilitySlider.value = defaultAgility;
                    dynamicsSwitch.checked = true;
                }
                background: Rectangle {
                     implicitWidth: 100
                     implicitHeight: 40
                     opacity: enabled ? 1 : 0.3
                     border.color: control.down ? "#ffffff" : "#cccccc"
                     border.width: height*0.05
                     radius: height*0.05
                }
            }
        }
    }
    Component.onCompleted: requestInputDeviceList()
}
