/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10
import QtQuick.Controls 2.3
import Qt.labs.settings 1.0
// Import translation module
import PitchMonitor 1.0

ApplicationWindow {
    id: mainwindow
    visible: true

    //-------------------------- Application window ---------------------------

    readonly property string version: "1.1"
    property string defaultAppname: qsTr("Choral Pitch Monitor");
    title: ""
    color: "black"
    width: 640
    height: 480

    //------------------------- Language selection ----------------------------


    property bool asianLanguage: (translator.index>=6)

    Translator {
        id: translator
        onIndexChanged: requestInputDeviceList()
    }

    //--------------------------- Audio input system --------------------------

    // VARIABLES
    property double volume: 0
    property string inputDeviceName: "automatic"
    onInputDeviceNameChanged: { connectAudioDevice(inputDeviceName) }
    property var deviceList: [qsTr("searching...")]
    property int deviceNumber: 0
    // SIGNALS
    signal requestInputDeviceList()
    signal connectAudioDevice (string name)
    // SLOTS
    function slotDisplayVolume(vol,dummy) { volume=vol; }
    function slotRequestAudioConnection() { connectAudioDevice(inputDeviceName); }
    function slotUpdateDeviceList (list,index) { deviceList=list; deviceNumber=index; }

    //---------------------------- Error messages -----------------------------

    function showError (msg) { error.text = msg; if (!error.visible) errorAnimation.start(); }
    function showAlert(msg) { if (stack.currentItem.objectName == "mainview") showError(msg); }

    //--------------------------- Reference frequency -------------------------

    property int referenceFrequency: 440
    signal measureReferenceFrequency()
    signal setReferenceFrequency (double value)
    function slotSetReferenceFrequency(value) { referenceFrequency=value; }

    //-------------------- Actual pitch and intonation alarm ------------------

    property double pitch: 0
    property int threshold: 30
    property bool intonationAlarm: false
    function slotGauge (value) { pitch=value; }
    onPitchChanged: intonationAlarm = (pitch < -threshold ? -1 : (pitch > threshold ? 1 : 0))
    function slotIntonationAlarm (active) { intonationAlarm=active; }
    signal resetGauge()

    //--------------------------- Squelch and agility --------------------------

    readonly property double defaultAgility: 0.3
    readonly property double defaultNoiseThreshold: 0.5
    property bool gaugeActive: false
    property double squelch: defaultNoiseThreshold
    property double agility: defaultAgility
    property bool noiseDetectionEnabled: true
    property bool dynamicalLevelEnabled: true
    property string level: ""

    function slotgaugeActive (isPlaying) { gaugeActive=isPlaying; }
    function slotSetDynamicalLevel (l) { level=l }
    signal setNoiseDetectorParameters(bool on, double value)
    signal setAgility(double value)

    onSquelchChanged: setNoiseDetectorParameters(noiseDetectionEnabled,squelch)
    onNoiseDetectionEnabledChanged: setNoiseDetectorParameters(noiseDetectionEnabled,squelch)
    onAgilityChanged: setAgility(agility)

    //--------------------------- C++ request ----------------------------------

    function slotRequestSettings() {
        setReferenceFrequency(referenceFrequency);
        setNoiseDetectorParameters(noiseDetectionEnabled,squelch);
        setAgility(agility);
    }

    //------------------- Free fonts used in this application ------------------

    FontLoader {
        id: headlineFont;
        source: "fonts/EuphoriaScript-Regular.ttf"
    }
    FontLoader {
        id: standardFont;
        source: "fonts/Typography-Times-Regular.ttf"
    }
    FontLoader {
        id: dynamicsFont;
        source: "fonts/Century-Condensed-Bold-Italic.ttf"
    }

    //-------------------------- Settings of the app --------------------------


    Settings {
        id: settings
        property alias x: mainwindow.x
        property alias y: mainwindow.y
        property alias width: mainwindow.width
        property alias height: mainwindow.height
        property alias title: mainwindow.title
        property alias referencePitch: mainwindow.referenceFrequency
        property alias threshold: mainwindow.threshold
        property alias inputDeviceName: mainwindow.inputDeviceName
        property alias squelch: mainwindow.squelch
        property alias noiseDetectionEnabled: mainwindow.noiseDetectionEnabled
        property alias agility: mainwindow.agility
        property alias dynamicalLevelEnabled: mainwindow.dynamicalLevelEnabled
    }




    // BACKGROUND BRUSHED ALUMINIUM

    Image {
        id: alu
        source: "qrc:/images/aluminium.jpg"
        anchors.fill: parent
    }


    // STACK VIEW SYSTEM

    StackView {
        id: stack
        initialItem: mainView
        anchors.fill: parent

        // Handle keypress
        focus: true
        Keys.onPressed: {
            var name = stack.currentItem.objectName
            switch(event.key) {
            case Qt.Key_Escape:
                if (name=="mainview") Qt.quit();
                else pop();
                break;

            case Qt.Key_T:
                if (name=="mainview") stack.currentItem.startTuning();
                break;

            case Qt.Key_F1:
            case Qt.Key_H:
                if (name=="mainview") push(helpView); else pop();
                break;

            case Qt.Key_F10:
            case Qt.Key_F12:
            case Qt.Key_S:
                if (name=="mainview") push(settingsView); else pop();
                break;

            case Qt.Key_Up:
                if (name!="mainview") stack.currentItem.flick(1);
                break;

            case Qt.Key_Down:
                if (name!="mainview") stack.currentItem.flick(-1);
                break;

            case Qt.Key_Left:
                if (name!="mainview") pop();
                break;

            case Qt.Key_Plus:
                if (name=="settingsview") stack.currentItem.modifyPitch(1);
                break;

            case Qt.Key_Minus:
                if (name=="settingsview") stack.currentItem.modifyPitch(-1);
                break;

            }
        }
        Keys.onReleased: {
            if (event.key === Qt.Key_Back) {
                //console.log("Android back key")
                if (stack.currentItem.objectName !== "mainview") {
                    event.accepted = true;
                    pop()
                }
                else {
                    console.log("Android back key ignored");
                    event.accepted = true;
                }
            }
        }



        pushEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 500
            }
        }
        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 500
            }
        }
        popEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 500
            }
        }
        popExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 500
            }
        }

    }

    // COMPONENTS IN THE STACK VIEW

    Component {
        id: mainView
        MainView {
            objectName: "mainview"
            onOpenSettings: stack.push(settingsView)
            onGetHelp: stack.push(helpView)
        }
    }

    Component {
        id: helpView
        HelpView { objectName: "helpview"; }
    }

    Component {
        id: settingsView
        SettingsView { objectName: "settingsview" }
    }

    Item {
        id: error
        visible: false
        anchors.centerIn: parent
        anchors.verticalCenterOffset: parent.height*0.05
        width: parent.width*0.7
        height: width*0.2;
        property alias text: msg.text

        Rectangle {
            color: "#222"
            anchors.fill: parent
            radius: width*0.05
            border.color: "white"
            border.width: 1
        }
        Text {
            id: msg
            color: "white"
            font.family: standardFont.name
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: parent.width*0.1 * Math.min(3.7/Math.sqrt(text.length),1);
            anchors.fill: parent
            anchors.margins: parent.width*0.05
            wrapMode: TextInput.Wrap
            lineHeight: 1.2
        }
    }

    PropertyAnimation {
        id: errorAnimation;
        target: error;
        easing.type: Easing.InOutQuad
        alwaysRunToEnd: true;
        property: "opacity";
        from: 0
        to: 1
        duration: 1000
        onStarted: error.visible = true
        onStopped: errorAnimation2.start()
    }

    PropertyAnimation {
        id: errorAnimation2;
        target: error;
        easing.type: Easing.InOutQuad
        alwaysRunToEnd: true;
        property: "opacity";
        from: 1
        to: 0
        duration: 4000
        onStarted: error.visible = true
        onStopped: error.visible = false
    }
}
