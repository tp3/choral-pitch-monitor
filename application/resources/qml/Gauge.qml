/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.3

Item {
    id: gauge
    property alias value: circularGauge.value
    property alias gaugeSize: circularGauge.gaugeSize
    property double anim: 0

    property bool intonationAlarm: mainwindow.intonationAlarm
    property bool gaugeActive: mainwindow.gaugeActive
    onIntonationAlarmChanged: if (intonationAlarm) animation.start();
    onGaugeActiveChanged: if (intonationAlarm && gaugeActive) animation.start();

    CircularGauge {
        id: circularGauge
        stepSize: 0.01
        maximumValue: 50
        minimumValue: -50
        anchors.fill: parent
        anchors.topMargin: 0.05*parent.height
        anchors.bottomMargin: 0.05*parent.height
        anchors.rightMargin: parent.width*0.05
        anchors.leftMargin: parent.width*0.05
        property int gaugeSize: Math.min(height,width)

        style: CircularGaugeStyle {
            minimumValueAngle: -150
            maximumValueAngle: 150

            tickmark: Rectangle {
                implicitWidth: outerRadius * 0.02
                antialiasing: true
                implicitHeight: outerRadius * 0.08
                color: Math.abs(styleData.value) > mainwindow.threshold ? "#ffb0b0" : "#b0ffb0"
                //color: "white"
            }
            minorTickmark: Rectangle {
                implicitWidth: outerRadius * 0.008
                antialiasing: true
                implicitHeight: outerRadius * 0.05
                color: Math.abs(styleData.value) > mainwindow.threshold ? "#ffb0b0" : "#b0ffb0"
                //color: "white"
            }
            tickmarkLabel: Text {
                text: styleData.value
                style: Text.Raised; styleColor: "black"
                font.family: standardFont.name;
                font.pixelSize: Math.max(10,outerRadius*0.1)
                color: "white"
            }
        }

        Behavior on value {
               SmoothedAnimation { velocity: 50 }
        }
    }

    // INTONATION ALARM (UP AND DOWN ARROWS)

    Item {
        id: arrows
        property int gaugeSize: Math.min(circularGauge.height,circularGauge.width)
        anchors.top: circularGauge.top
        anchors.topMargin: (circularGauge.height-gaugeSize)/2+gaugeSize/8;
        height: gaugeSize/3
        anchors.horizontalCenter: circularGauge.horizontalCenter
        width: height

        Image {
            id: up
            anchors.fill: parent
            visible: intonationAlarm && gauge.value < 0 && gauge.width <=1.4*gauge.height
            fillMode: Image.PreserveAspectFit
            source: "qrc:/icons/up.png"
            smooth: true
            mipmap: true
            opacity: 4*anim*(1-anim)
        }

        Image {
            id: down
            anchors.fill: parent
            visible: intonationAlarm && gauge.value > 0
            opacity: up.opacity
            fillMode: Image.PreserveAspectFit
            source: "qrc:/icons/down.png"
            smooth: true
            mipmap: true
        }
    }

    Image {
        id: upleft
        visible: intonationAlarm && gauge.value < -25 && gauge.width > 1.4*gauge.height
        source: "qrc:/icons/up.png"
        anchors.bottom: gauge.bottom
        anchors.left: gauge.left
        smooth: true
        mipmap: true
        fillMode: Image.PreserveAspectFit
        anchors.bottomMargin: parent.height*anim*0.5
        anchors.leftMargin: parent.width*0.2-circularGauge.height*0.33
        width: parent.width * 0.2
        height: width
        opacity: 4*anim*(1-anim)
    }

    Image {
        id: upright
        visible: upleft.visible
        source: "qrc:/icons/up.png"
        anchors.bottom: gauge.bottom
        anchors.right: gauge.right
        smooth: true
        mipmap: true
        fillMode: Image.PreserveAspectFit
        anchors.bottomMargin: parent.height*anim*0.5
        anchors.rightMargin: parent.width*0.2-circularGauge.height*0.33
        width: parent.width * 0.2
        height: width
        opacity: upleft.opacity
    }

    PropertyAnimation {
        id: animation;
        target: gauge;
        easing.type: Easing.InOutQuad
        alwaysRunToEnd: true;
        property: "anim";
        from: 0
        to: 1;
        duration: 2000
        onStopped: if (intonationAlarm && gaugeActive) animation.start()
    }

}
