import QtQuick 2.10
import QtQuick.Controls 2.4

ToolTip {
    delay: 1500
    timeout: 4000
    height: mainwindow.width*0.04
    topPadding: height*0.25
    leftPadding: height*0.2
    rightPadding: height*0.2
    font.pixelSize: height*0.66
    font.family: standardFont.name
    background: Rectangle {
              implicitWidth: 2
              implicitHeight: 2
              color: "#ffd180"
              border.color: "#ff5500"
              radius: parent.height*0.3
    }
}
