/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: tuningmessage
    visible: true
    anchors.centerIn: parent
    height: parent.height*0.5
    width: parent.width*0.7
    property bool waiting: false

    function start() {
        if (visible) {
            if (waiting) {
                animationZero.stop();
                animationOne.stop();
                tuningmessage.opacity=1;
                waiting=false;
            }
            return;
        }
        visible = true;
        opacity = 0;
        bar.opacity = 1;
        bar.col="darkred";
        bar.value=1;
        bottomline.text=qsTr ("Please play a few notes...");
        animationZero.start()
        waiting = true;
    }

    Image {
        id: plate
        anchors.fill: parent
        source: "qrc:/images/plate.png"
    }

    Text {
        id: hidden
        anchors.fill: plate
        color: "white"
        text: referenceFrequency + " Hz"
        opacity: 1-bar.opacity
        font.family: standardFont.name
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: Math.min(parent.height*0.17, parent.width*0.1)
    }


    ProgressBar {
        id: bar
        property color col: "darkred"
        anchors.top: parent.top
        anchors.topMargin: parent.height * 0.4
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: parent.width*0.1
        anchors.rightMargin: parent.width*0.1
        height: Math.min(parent.width*0.05,parent.height*0.1)
        orientation: Qt.Horizontal

        style: ProgressBarStyle {
            background: Rectangle {
                radius: height*0.3
                color: "lightgray"
                border.color: "gray"
                border.width: 1
                implicitWidth: 200
                implicitHeight: 24
            }
            progress: Rectangle {
                radius: height*0.3
                color: bar.col
                border.color: "white"
            }
        }

    }

    PropertyAnimation {
        id: animationZero
        target: tuningmessage
        //alwaysRunToEnd: true
        property: "opacity"
        from: 0
        to: 1
        duration: 700
        onStopped: {
            animationOne.start();
        }
    }

    PropertyAnimation {
        id: animationOne
        target: bar
        //alwaysRunToEnd: true
        property: "value"
        from: 1
        to: 0
        duration: 3000
        onStopped: {
            bar.col="green";
            bottomline.text=qsTr ("Measuring reference pitch...");
            animationTwo.start();
        }
    }

    PropertyAnimation {
        id: animationTwo;
        target: bar;
        alwaysRunToEnd: true;
        property: "value";
        from: 0
        to: 1;
        duration: 5000
        onStopped: {
            mainwindow.measureReferenceFrequency();
            bottomline.text=qsTr ("New reference pitch");
            animationThree.start();
        }
    }

    PropertyAnimation {
        id: animationThree;
        target: bar;
        alwaysRunToEnd: true;
        property: "opacity";
        to: 0;
        duration: 2000
        onStopped: { animationFour.start(); }
    }

    PropertyAnimation {
        id: animationFour;
        target: bar;
        alwaysRunToEnd: true;
        property: "opacity";
        to: 0;
        duration: 2000
        onStopped: { animationFive.start(); }
    }

    PropertyAnimation {
        id: animationFive;
        target: tuningmessage;
        alwaysRunToEnd: true;
        property: "opacity";
        to: 0;
        duration: 700
        onStopped: { tuningmessage.visible=false; }
    }

    Text {
        id: headline
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: bar.top
        anchors.bottomMargin: parent.height * 0.1
        color: "white"
        text: qsTr ("Calibration")
        font.family: headlineFont.name
        font.pixelSize: Math.min(parent.height*0.2,parent.width*0.15)
        height: parent.height*0.2
    }

    Text {
        id: bottomline
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height * 0.2
        color: "white"
        text: "undefined"
        font.family: standardFont.name;
        font.pixelSize: Math.min(parent.height*0.17, parent.width*0.05)
    }

    MouseArea {
        anchors.fill: parent
        onClicked: parent.start()
    }
}
