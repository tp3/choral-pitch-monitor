/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.7
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0

Item {
    id: help

    function flick(direction) {
        console.log("flick",direction);
        flickable.flick(0,direction*200);
    }


    TopLine {
        id: headline
        headline: qsTr("About this App");
    }

    RectangularGlow {
        id: effect
        opacity: 0.2
        anchors.fill: field
        glowRadius: field.radius
        spread: 0.3
        color: "white"
        cornerRadius: field.radius + glowRadius
    }

    Rectangle {
        id: field
        color: "black"
        opacity: 0.3
        anchors.top: headline.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: parent.width*0.04
        radius: parent.width*0.015;
    }

    // FLICKABLE SETTINGS REGION

    Flickable {
        id: flickable
        anchors.fill: field
        anchors.margins: parent.width*0.03
        contentHeight: Math.max(width*1.05+height*0.23,height*1.2)
        contentWidth: width
        clip: true

        // APP ICON TUNINGFORK

        Image {
            id: tuningfork
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: parent.width * 0.05
            width: parent.width*0.15
            height: width
            smooth: true
            source: "qrc:/icons/tuningfork.png"
        }

        Glow {
              anchors.fill: tuningfork
              radius: tuningfork.width*0.3
              samples: 30
              color: "white"
              source: tuningfork
              opacity: 0.7
          }

        DropShadow {
            anchors.fill: tuningfork
            horizontalOffset: tuningfork.width*0.02
            verticalOffset: tuningfork.width*0.02
            radius: tuningfork.width*0.05
            samples: 17
            color: "#40000000"
            source: tuningfork
        }

        Text {
            id: apptiletext
            text: mainwindow.defaultAppname
            anchors.top: tuningfork.bottom
            anchors.topMargin: parent.width*0.05
            anchors.left: parent.left
            anchors.right: parent.right
            font.family: standardFont.name
            font.pixelSize:width * 0.06
            horizontalAlignment: Text.AlignHCenter
            color: "white"
        }

        Text {
            lineHeight: 1.3
            id: license
            textFormat: Text.RichText
            linkColor: "red"
            text: "Haye Hinrichsen, " +
            " <a href=\"https://tp3.app\" style=\"color: rgb(255,128,128)\">TP III</a>, " +
            qsTr("University of Würzburg, Germany") + "<br>" +
            qsTr("Free software based on") +
            " <a href=\"https://www.qt.io\" style=\"color: rgb(255,128,128)\">Qt</a> " +
            qsTr("and") +
            " <a href=\"http://www.fftw.org\" style=\"color: rgb(255,128,128)\">FFTW3</a> " + "<br>" +
            qsTr("licensed under") +
            " <a href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\" style=\"color: rgb(255,128,128)\">GNU GPLv3.</a> " + qsTr("Version") + " " + mainwindow.version
            anchors.top: apptiletext.bottom
            anchors.topMargin: parent.width*0.02
            anchors.left: parent.left
            anchors.right: parent.right
            font.family: standardFont.name
            font.pixelSize:width * 0.03
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            onLinkActivated: Qt.openUrlExternally(link);
        }

        Text {
            textFormat: Text.RichText
            lineHeight: 1.27
            text: "<b>" + qsTr("Usage:") + " </b>" +
                  qsTr("Choirs often suffer from the problem of a collective pitch drift.") + " " +
                  qsTr("This app allows you to monitor the average pitch deviation of all voices while singing.") + " " +
                  qsTr("Warning signs will appear if the pitch deviation becomes too large.") + " " +
                  qsTr("To define the reference pitch simply tap the tuning fork and play a few notes.") + "<br><br>" +
                  qsTr("More information at") +
                  " <a href=\"https://pitchmonitor.tp3.app\" style=\"color: rgb(255,128,128)\">pitchmonitor.tp3.app</a>."
            wrapMode: TextInput.Wrap
            anchors.top: license.bottom
            anchors.topMargin: parent.width*0.05
            anchors.left: parent.left
            anchors.right: parent.right
            font.family: standardFont.name
            font.pixelSize: mainwindow.width * 0.023 + mainwindow.height * 0.01
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            onLinkActivated: Qt.openUrlExternally(link);
        }
    }
}
