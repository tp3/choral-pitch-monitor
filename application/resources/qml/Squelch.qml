import QtQuick 2.10
import QtGraphicalEffects 1.0

Item {
    id: squelchArea
    property bool enabled: mainwindow.noiseDetectionEnabled
    property bool gaugeActive: mainwindow.gaugeActive
    property bool dynamicLevelEnabled: mainwindow.dynamicalLevelEnabled && mainwindow.level.length>0

    RectangularGlow {
        id: effect
        anchors.fill: indicator
        glowRadius: 20
        spread: 0.3
        color: "white"
        cornerRadius: indicator.radius + glowRadius
        opacity: (indicator.opacity+0.3)/1.3
    }

    Rectangle {
        id: indicator
        anchors.centerIn: parent
        height: squelchArea.width * 0.2
        width: height
        border.width: 1
        radius: width*0.8
        color: "red"
        border.color: "white"
        scale: (mouseArea.containsMouse ? 1.2:1)
    }

    MouseArea
    {
        id: mouseArea
        anchors.fill: squelchArea
        hoverEnabled: true
        onClicked: mainwindow.noiseDetectionEnabled = ! mainwindow.noiseDetectionEnabled // Toggle
    }

    MyToolTip {
        text: dynamicLevelEnabled ? qsTr("Volume indicator") : qsTr("Noise detector");
        visible: mouseArea.containsMouse
        y: mouseArea.y
     }


    PropertyAnimation {
        id: showIndicatorAnimation;
        target: indicator;
        easing.type: Easing.InOutQuad
        property: "opacity";
        //from: 0
        to: 1
        duration: 300
    }

    PropertyAnimation {
        id: hideIndicatorAnimation;
        target: indicator;
        easing.type: Easing.InOutQuad
        property: "opacity";
        //from: 1
        to: 0
        duration: 500
    }

    PropertyAnimation  {
        id: greenIndicatorAnimation
        target: indicator
        properties: "color"
        //from: "red"
        to: "#80ff80"
        duration: 1000
    }

    PropertyAnimation {
        id: redIndicatorAnimation
        target: indicator
        properties: "color"
        //from: "#80ff80"
        to: "red"
        duration: 1000
    }

    PropertyAnimation {
        id: showDynamicsAnimation;
        target: dynamics;
        easing.type: Easing.InOutQuad
        property: "opacity";
        //from: 0
        to: 1
        duration: 1000
    }

    PropertyAnimation {
        id: hideDynamicsAnimation;
        target: dynamics;
        easing.type: Easing.InOutQuad
        property: "opacity";
        //from: 1
        to: 0
        duration: 1000
    }

    onEnabledChanged: {
        //console.log("enabled changed",enabled)
        animate();
    }

    onGaugeActiveChanged: {
        //console.log("active changed",gaugeActive);
        animate();
        if (gaugeActive) greenIndicatorAnimation.start();
        else redIndicatorAnimation.start()
    }

    onDynamicLevelEnabledChanged: {
        //console.log("dynamicLevelEnabled changed",dynamicLevelEnabled);
        animate();
     }

    function animate() {
        if (dynamicLevelEnabled && (!enabled || enabled && gaugeActive)) showDynamicsAnimation.start();
        else hideDynamicsAnimation.start();
        if (!dynamicLevelEnabled && enabled) showIndicatorAnimation.start();
        else hideIndicatorAnimation.start();
    }

    Text {
        property int displacement: parent.height*0.03
        color: "black"
        text: mainwindow.level
        anchors.fill: parent
        anchors.topMargin: -parent.width*0.2+displacement
        anchors.bottomMargin:-displacement
        anchors.leftMargin: displacement
        anchors.rightMargin: -displacement
        font.family: dynamicsFont.name
        verticalAlignment: Text.AlignBottom
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: height*0.8
        style: Text.Raised; styleColor: "white"
        opacity: dynamics.opacity
    }
    Text {
        id: dynamics
        color: "#ddd"
        text: mainwindow.level
        anchors.fill:parent
        anchors.topMargin: -parent.width*0.2
        font.family: dynamicsFont.name
        verticalAlignment: Text.AlignBottom
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: height*0.8
        style: Text.Sunken; styleColor: "black"
        opacity: 1
    }
}
