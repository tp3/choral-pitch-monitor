/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10
import QtQuick.Controls 2.4

SpinBox {
    id: control
    font.pixelSize: height*0.55
    font.family: standardFont.name
    wheelEnabled: true

    contentItem: TextInput {
        text: control.textFromValue(control.value, control.locale)
        topPadding: 6
        bottomPadding:  0
        height: control.height
        font: control.font
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: Qt.ImhFormattedNumbersOnly
    }

     up.indicator: Rectangle {
         x: control.mirrored ? 0 : parent.width - width
         height: parent.height
         width: parent.height*1.4
         implicitWidth: 10
         implicitHeight: 10
         border.width: height*0.04
         border.color: "#808080"

         Text {
             text: "+"
             color: "black"
             anchors.fill: parent
             font: control.font
             horizontalAlignment: Text.AlignHCenter
             verticalAlignment: Text.AlignVCenter
         }
     }

     down.indicator: Rectangle {
         x: control.mirrored ? parent.width - width : 0
         height: parent.height
         width: parent.height*1.4
         implicitWidth: 10
         implicitHeight: 10
         color: control.down.pressed ? "#e4e4e4" : "#f6f6f6"
         border.width: height*0.04
         border.color: "#808080"


         Text {
             text: "-"
             font.pixelSize: height*0.65
             font.family: standardFont.name
             color: "black"
             anchors.fill: parent
             horizontalAlignment: Text.AlignHCenter
             verticalAlignment: Text.AlignVCenter
         }
     }

     background: Rectangle {
         implicitWidth: 140
         border.color: "#c0c0c0"
         border.width: height*0.04
     }

}

