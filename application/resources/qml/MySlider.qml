/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0

Slider {
    id: mySlider
    handle: Rectangle {
        x: mySlider.leftPadding + mySlider.visualPosition * (mySlider.availableWidth - width)
        y: mySlider.topPadding + mySlider.availableHeight / 2 - height / 2
        implicitWidth: squelchSlider.height*0.9
        implicitHeight: squelchSlider.height*0.9
        radius: implicitWidth/2
        color: mySlider.pressed ? "#f0f0f0" : "#c0c0c0"
        border.color: "#ffffff"
        border.width: parent.height*0.02
    }
    background: Rectangle {
        x: mySlider.leftPadding
        y: mySlider.topPadding + mySlider.availableHeight / 2 - height / 2
        implicitWidth: mySlider.height
        implicitHeight: mySlider.height/5
        width: mySlider.availableWidth
        height: implicitHeight
        radius: implicitHeight/3
        color: "#bdbebf"
        border.color: "#ffffff"
        border.width: parent.height*0.02

        Rectangle {
            width: mySlider.visualPosition * parent.width
            height: parent.height
            color: "#000000"
            radius: height/3
            border.color: "#ffffff"
            border.width: parent.border.width
        }
    }
}
