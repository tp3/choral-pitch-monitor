/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10

Item {
//    id:win
//    onWidthChanged: console.log(win.width,win.height)
//    onHeightChanged: console.log(win.width,win.height)

    property bool tuning: false
    signal openSettings()
    signal getHelp()
    function startTuning() { tuningmessage.start(); }

    ControlBar {
        id: controlbar
        anchors.fill: parent
        anchors.bottomMargin: parent.height - Math.min(parent.width,parent.height) * 0.12
    }

    Text {
        id: text
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: gauge.verticalCenter
        anchors.verticalCenterOffset: -0.17 * Math.min(gauge.width,gauge.height)
        style: Text.Sunken; styleColor: "white"
        font.family: headlineFont.name;
        font.pixelSize: gauge.gaugeSize*0.09
        color: "#505050"
        text: "cents"
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea
    {
        id: mouseArea
        anchors.fill: text
        hoverEnabled: true
    }

    MyToolTip {
        y: mouseArea.y
        text: qsTr("A cent equals 1/100 of a semitone");
        visible: mouseArea.containsMouse
     }


    Gauge {
        id: gauge
        visible: ! tuning
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: controlbar.bottom
        anchors.bottom: parent.bottom
        value: pitch
    }

    MouseArea {
        id: axis
        anchors.centerIn: gauge
        height: Math.min(gauge.width,gauge.height) * 0.17
        width: height
        onClicked: mainwindow.resetGauge();
        hoverEnabled: true
    }

    MyToolTip {
        y: axis.y
        text: qsTr("Click at the axis to reset gauge");
        visible: axis.containsMouse
    }

//    Rectangle {
//        color: "green"
//        anchors.fill: axis
//        opacity: 0.5
//    }

    Squelch {
        id: squelch
        anchors.centerIn: gauge
        height: Math.min(gauge.width,gauge.height) * 0.15
        anchors.verticalCenterOffset: height
        width: height
    }


    MyButton {
        id: tuningbutton
        width: Math.max(gauge.height*0.1,gauge.gaugeSize*0.2)
        height: width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: squelch.bottom
        source: "qrc:/icons/tuningfork.png"
        onClicked: tuningmessage.start()
        tooltip: qsTr("Tap Tuning Fork to define Reference Frequency")
        tooltipEnabled: ! tuningmessage.visible
    }

    Text {
        id: frequency
        style: Text.Raised; styleColor: "black"
        visible: ! tuning
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.02
        font.family: standardFont.name;
        font.pixelSize: gauge.gaugeSize*0.06
        color: "white"
        text: referenceFrequency +" Hz"
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea
    {
        id: mouseArea2
        anchors.fill: frequency
        hoverEnabled: true
    }

    MyToolTip {
        text: qsTr("Reference frequency");
        visible: mouseArea2.containsMouse
        y: mouseArea2.y
     }



    TuningMessage {
        id: tuningmessage
        visible: false
    }
}
