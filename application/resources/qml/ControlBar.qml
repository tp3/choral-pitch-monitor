/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10
import QtQuick.Controls 1.6

Item {
    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.7
    }

    Text {
        id: headline
        anchors.centerIn: parent
        style: Text.Sunken; styleColor: "white"
        color: "gray"
        text: mainwindow.title=="" ? defaultAppname : mainwindow.title
        font.family: headlineFont.name
        font.pixelSize: (mainwindow.asianLanguage ? 0.7:1)*
                        Math.min(parent.height*0.95,parent.width/text.length*1.5)
    }

    MyButton {
        width: height
        imageOpacity: 0.5
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        margins: parent.height*0.2
        source: "qrc:/icons/question.png"
        tooltip: qsTr("Information and Help")
        onClicked: getHelp()
    }

    MyButton {
        width: height
        imageOpacity: 0.5
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        margins: parent.height*0.2
        tooltip: qsTr("Enter Settings")
        source: "qrc:/icons/settings.png"
        onClicked: openSettings()
    }
}
