/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4

Item {
    property string headline: "title undefined"

    width: parent.width
    height: width*0.1


    Text {
        id: settingstitle
        width: parent.width
        color: "white"
        text: headline
        style: Text.Raised; styleColor: "black"
        font.family: standardFont.name
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: parent.width*0.06
        anchors.top: parent.top
        anchors.topMargin: parent.width*0.05
    }

    // GO BACK Buttons

    Item {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: parent.width*0.12

        MyButton {
            margins: parent.width*0.02
            source: "qrc:/icons/back.png"
            anchors.left: parent.left
            height: parent.height
            width: height;
            tooltip: qsTr("Go back")
            onClicked: {
                stack.pop()
            }
        }

        MyButton {
            margins: parent.width*0.02
            source: "qrc:/icons/back.png"
            anchors.right: parent.right
            height: parent.height
            width: height;
            tooltip: qsTr("Go back")
            onClicked: {
                stack.pop()
            }
        }

    }


}
