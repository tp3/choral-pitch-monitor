/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.10
import QtQuick.Controls 2.4

Rectangle {
    property alias text: textfield.text
    property alias placeholderText: textfield.placeholderText

    border.color: "#a0a0a0"
    border.width: height*0.04
    color: "white"
    TextField {
        id: textfield
        anchors.fill: parent
        topPadding: 0
        bottomPadding: 0
        anchors.leftMargin: if (parent.height > 20) (parent.height-20)*0.3
        background: Rectangle {
             implicitWidth: 20
             implicitHeight: 40
             color: "transparent"
             border.color: "transparent"
         }
        text: mainwindow.title
        selectByMouse: true
        font.pixelSize: field.width*0.04
        font.family: standardFont.name
        onTextChanged: mainwindow.title=text
    }
}
