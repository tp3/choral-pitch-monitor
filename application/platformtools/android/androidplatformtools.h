/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef ANDROIDPLATFORMTOOLS_H
#define ANDROIDPLATFORMTOOLS_H

#include "../platformtools.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Derived class of the platform tools for Android
///////////////////////////////////////////////////////////////////////////////

class AndroidPlatformTools : public PlatformTools
{
public:
    AndroidPlatformTools() {}

    void keepScreenOn(bool on);
    bool requestPermissions(QVector<PlatformPermission> permissions);
};


// This will be the instance of the AndroidPlatformTools:
extern PlatformToolsInstanciator<AndroidPlatformTools> AndroidPlatformToolsInstance;


#endif // ANDROIDPLATFORMTOOLS_H
