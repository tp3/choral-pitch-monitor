/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include <QHash>
#include <QAndroidJniObject>
#include <QDebug>

#include "androidplatformtools.h"


// Create an instance of the Android platform tools

PlatformToolsInstanciator<AndroidPlatformTools> AndroidPlatformToolsInstance;


///////////////////////////////////////////////////////////////////////////////
/// \brief Keep the screen permanently on.
/// \param on : true if the screen shall be kept on
/// \note https://stackoverflow.com/questions/27758499/how-to-keep-the-screen-on-in-qt-for-android
///////////////////////////////////////////////////////////////////////////////

//template void QAndroidJniObject::callMethod<void> (const char *methodName, const char *sig, int) const;

void AndroidPlatformTools::keepScreenOn(bool on)
{
    QtAndroid::runOnAndroidThread([on]{
        QAndroidJniObject activity = QtAndroid::androidActivity();
        if (activity.isValid())
        {
            QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
            if (window.isValid()) {
                const int FLAG_KEEP_SCREEN_ON = 128;
                if (on) {
                    window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
                } else {
                    window.callMethod<void>("clearFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
                }
            }
        }
        QAndroidJniEnvironment env;
        if (env->ExceptionCheck()) {
            env->ExceptionClear();
        }
    });
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Request specific permission on Android.
/// \param permissions (according to the enum Permissions in the base class)
/// \note http://www.bytran.org/androidmpermissions.htm
///////////////////////////////////////////////////////////////////////////////

bool AndroidPlatformTools::requestPermissions (QVector<PlatformPermission> permissions)
{
    for (PlatformPermission permission : permissions)
    {
        QString permissionString;
        switch (permission)
        {
            case PlatformPermission::microphone: permissionString = "android.permission.RECORD_AUDIO"; break;
        }
        // Ask Android whether this permission has been granted
        QtAndroid::PermissionResult request = QtAndroid::checkPermission(permissionString);
        // if not:
        if (request == QtAndroid::PermissionResult::Denied)
        {
            QtAndroid::requestPermissionsSync(QStringList() << permissionString);
            request = QtAndroid::checkPermission(permissionString);
            if (request == QtAndroid::PermissionResult::Denied) return false;
        }
    }
    return true;
}

