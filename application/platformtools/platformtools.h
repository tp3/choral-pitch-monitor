/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef PLATFORMTOOLS_H
#define PLATFORMTOOLS_H

#include <QObject>
#include <assert.h>
#include <memory>
#include <QVector>

class Application;


enum class PlatformPermission {microphone};


///////////////////////////////////////////////////////////////////////////////
/// \brief Base class providing support for various platforms
/// \details This singleton class is used to access platform-dependent
/// components. The platform-dependent components are defined as derived
/// casses named XXXPlatformTools. To create an instance of these tools
/// one has to instantiate a global object of the type
/// \code PlatformToolsInstanciator<XXXPlatformTools> instance;
/// \endcode
///////////////////////////////////////////////////////////////////////////////

class PlatformTools : public QObject
{
    Q_OBJECT
protected:
    PlatformTools() {}
public:
    virtual ~PlatformTools() {}

public:

    ///////////////////////////////////////////////////////////////////////////
    /// \brief Platform-dependent tools
    /// \details To access platform-dependent functions declare them as virtual
    /// functions here in this base class, override them in the corresponding
    /// platform-dependent derived class and access them by calling
    /// \code PlatformTools::getSingleton().<functionname>(<arguments>);
    /// \endcode
    ///////////////////////////////////////////////////////////////////////////

    static PlatformTools &getSingleton()
    {
        assert(getPointerToInstance());
        return *getPointerToInstance();
    }
    //=========================================================================

    ///////////////////////////////////////////////////////////////////////////
    /// \brief Platform-dependent initialization
    ///////////////////////////////////////////////////////////////////////////

    virtual void init (Application &application) {(void)application;}


    ///////////////////////////////////////////////////////////////////////////
    /// \brief Function to enable and disable the screen saver.
    ///////////////////////////////////////////////////////////////////////////

    virtual void keepScreenOn (bool on) { (void)on; }


    ///////////////////////////////////////////////////////////////////////////
    /// \brief Function to ask for specific permissions
    ///////////////////////////////////////////////////////////////////////////

    virtual bool requestPermissions (QVector<PlatformPermission> permissions)
    {   (void)permissions; return true;   }

    bool requestPermissions (PlatformPermission permission)
    {
        QVector<PlatformPermission> permissions;
        permissions.append (permission);
        return requestPermissions(permissions);
    }


    //=========================================================================

public:

    ///////////////////////////////////////////////////////////////////////////
    /// \brief Function holding the instance pointer as a static unique pointer
    ///////////////////////////////////////////////////////////////////////////

    static std::unique_ptr<PlatformTools> &getPointerToInstance()
    {
        static std::unique_ptr<PlatformTools> mInstance(new PlatformTools);
        return mInstance;
    }
};

//#############################################################################


///////////////////////////////////////////////////////////////////////////////
/// \brief Helper class for instantiating objects derived from PlatformTools
/// \details Usage: in the cpp file put the line
/// \code PlatformToolsInstanciator<XXXPlatformTools> XXXPlatformToolsInstance;
/// \endcode
///////////////////////////////////////////////////////////////////////////////

template <class T>
class PlatformToolsInstanciator {
public:
    friend class PlatformTools;
    PlatformToolsInstanciator()
    {
        PlatformTools::getPointerToInstance().reset(new T());
    }
};

#endif // PLATFORMTOOLS_H

