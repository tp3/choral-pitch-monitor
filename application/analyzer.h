/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef ANALYZER_H
#define ANALYZER_H

#include <QTimer>
#include <QFuture>
#include <QFile>

#include "audioinput.h"
#include "fouriertransform.h"
#include "noisedetector.h"

//=============================================================================
//                             Class Analyzer
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \class Analyzer
/// \brief Main part of the pitch monitor that analyzes the incoming sound.
///////////////////////////////////////////////////////////////////////////////

class Analyzer : public QObject
{
    Q_OBJECT

    using complex = std::complex<double>;
public:
    explicit Analyzer(AudioInput &audioInput);

signals:
    void signalGauge (QVariant pitch);                  ///< Send new value to the circular gauge
    void signalRequestSettings();                       ///< Request settings from QML
    void signalSetReferenceFrequency (QVariant pitch);  ///< Submit reference frequency to QML
    void signalIntonationAlarm (QVariant on);           ///< Activate/deactivate pitch warning (arrows)
    void signalGaugeEnabled (QVariant isPlaying);       ///< Activate/deactivate circular gauge
    void showAlert (QVariant msg);                      ///< Show a hint or a warning
    void showError (QVariant msg);                      ///< Display an error message

public slots:
    void start();
    void stop();
    void enable (QVariant on);
    void measureReferenceFrequency();
    void setReferenceFrequency (double f);
    void setAgility (double f);
    void resetGauge();

private:
    bool init(int msec);                    // Initialization of the analyzer
    void setGauge (complex phase);          // Set gauge meter

private slots:
    void timeout();                         // Timeout function for analysis

private:
    const int updateInterval = 100;         // Update interval in msec
    const double f0 = 440;                  // Default reference frequency

    QTimer *pTimer;                         ///< Timer triggering the analysis
    AudioInput &rAudioInput;                ///< Reference to the audio input module
    int mSampleSize;                        ///< Number of samples to be evaluated
    int mSampleRate;                        ///< Current sample rate
    QVector<double> mAudioBuffer;           ///< Window of the actual audio buffer
    QVector<double> mEnvelope;              ///< Envelope to round off the onset
    FourierTransform mFFT;                  ///< Standard FFT, non-optimized
    QVector<complex> mFFTBuffer;            ///< Array holding the FFT
    QVector<complex> mFilter;               ///< Filter function used for convolution
    QVector<int> mKey;                      ///< Key number ranging from 0 (C) to 11 (B)
    complex mReferencePhase;                ///< Reference phase for tuning
    bool mResetPhaseTrackingFlag;           ///< Flag to reset phase tracking
    QVector<complex> mNormalizedPhases;     ///< Memorized normalized phases (for measuring pitch)
    bool mEnabled;                          ///< Flag for enabling the gauge
    complex mAdiabaticGauge;                ///< Mollified gauge parameter
    double mAgility;                        ///< Parameter of gauge agility in [0,1]
};

#endif // ANALYZER_H
