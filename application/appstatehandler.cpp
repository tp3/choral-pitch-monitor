/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                          Application State Handler
//=============================================================================

#include "appstatehandler.h"
#include <QObject>
#include <QDebug>

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of an Application sate handler
/// \param QGuiApplication app
///////////////////////////////////////////////////////////////////////////////

AppStateHandler::AppStateHandler (QGuiApplication &app)
    : QObject(nullptr)
    , previousState(Qt::ApplicationActive)
{
    qRegisterMetaType<Qt::ApplicationState> ("Qt::ApplicationState");
    connect (&app,&QGuiApplication::applicationStateChanged,
             this,&AppStateHandler::applicationStateChanged);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: applicationStateChanged, emitting signals
/// for suspending and resuming the application.
/// \param state : Enum describing the new state of the application
///////////////////////////////////////////////////////////////////////////////

void AppStateHandler::applicationStateChanged(Qt::ApplicationState state)
{
    if (state != previousState)
    {
        if (state == Qt::ApplicationActive)
        {
            qInfo() << "Send request to resume application";
            emit resume();
        }
        else if (state == Qt::ApplicationSuspended)
        {
            qInfo() << "Send request to suspend application";
            emit suspend();
        }
        previousState = state;
    }
}


