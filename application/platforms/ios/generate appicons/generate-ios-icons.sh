echo
echo "Generate all required icons for IOS"
echo "After generating these files assign them via Xcode"
echo
#
CMD="convert icon-source.png -alpha Set -colorspace sRGB -define png:format=png32 -resize "
PRE="../Images.xcassets/AppIcon.appiconset"
#
echo "Deleting all previous icons"
rm $PRE/*.png
echo "Done"
echo
#
# The App Store icon:
$CMD 1024x1024 AppIcon-0-0-appstore-icon.png

# first row
$CMD 40x40 AppIcon-1-1-iphone-notification-ios-7-20pt-2x.png
$CMD 60x60 AppIcon-1-2-iphone-notification-ios-7-20pt-3x.png
$CMD 58x58 AppIcon-1-3-iphone-ios-7-29pt-2x.png
$CMD 87x87 AppIcon-1-4-iphone-ios-7-29pt-3x.png

#second row 
$CMD 80x80   AppIcon-2-1-iphone-spotlight-ios-7-40pt-2x.png
$CMD 120x120 AppIcon-2-2-iphone-spotlight-ios-7-40pt-3x.png
$CMD 120x120 AppIcon-2-3-iphone-app-ios-7-60pt-2x.png
$CMD 180x180 AppIcon-2-4-iphone-app-ios-7-60pt-3x.png

#third row
$CMD 20x20 AppIcon-3-1-ipad-notification-ios-7-20pt-1x.png
$CMD 40x40 AppIcon-3-2-ipad-notification-ios-7-20pt-2x.png
$CMD 29x29 AppIcon-3-3-ipad-settings-ios-7-29pt-1x.png
$CMD 58x58 AppIcon-3-4-ipad-settings-ios-7-29pt-2x.png

#last row
$CMD 40x40   AppIcon-4-1-ipad-spotlight-ios-7-40pt-1x.png
$CMD 80x80   AppIcon-4-2-ipad-spotlight-ios-7-40pt-2x.png
$CMD 76x76   AppIcon-4-3-ipad-app-ios-7-76pt-1x.png
$CMD 152x152 AppIcon-4-4-ipad-app-ios-7-76pt-2x.png
$CMD 167x167 AppIcon-4-5-ipad-pro-ios-9-83p5-2x.png


echo
echo "done."
exit

