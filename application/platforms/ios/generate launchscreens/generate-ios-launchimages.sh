echo
echo "Generate all required launch screen files for IOS."
echo "Requires a source file of size 3000x2400, ratio 0.8."
echo
echo "The launch screens are first created by running this script."
echo "Then the files are transferred by drag-and-drop to Xcode."
echo "Install the corresponding xcassets folder first."
echo "Activate also iPhone checkboxes in the right toolbar."
echo
CMD="convert Launch-Image-Source.jpg -alpha Set -colorspace sRGB -define png:format=png32 -gravity Center -crop "
#

# first row:
$CMD 37%x100% -resize 1242x2688! 1-1-LaunchImage-iOS12-iPhone-XSMax-Portrait.png
$CMD 37%x100% -resize 828x1792!  1-2-LaunchImage-iOS12-iPhone-XR-Portrait.png
$CMD 100%x58% -resize 2688x1242! 1-3-LaunchImage-iOS12-iPhone-XSMax-Landscale.png
$CMD 100%x58% -resize 1792x828!  1-4-LaunchImage-iOS12-iPhone-XR-Landscape.png

# second row:
$CMD 37%x100% -resize 1125x2436! 2-1-LaunchImage-iOS11-iPhone-X-XS-Portrait.png 
$CMD 100%x58% -resize 2436x1125! 2-2-LaunchImage-iOS11-iPhone-X-XS-Lanscape.png 

# third row:
$CMD 45%x100% -resize 1242x2208! 3-1-LaunchImage-iOS8-iPhone-RetinaHD55-Portrait.png
$CMD 45%x100% -resize 750x1334!  3-2-LaunchImage-iOS8-iPhone-RetinaHD47-Portrait.png
$CMD 100%x70% -resize 2208x1242! 3-3-LaunchImage-iOS8-iPhone-RetinaHD55-Landscape.png

# fourth row:
$CMD 53%x100% -resize 640x960!   4-1-LaunchImage-iOS7-iPhone-2x-Portrait.png
$CMD 45%x100% -resize 640x1136!  4-2-LaunchImage-iOS7-iPhone-Retina-4-Portrait.png

# iOS 7 iPad:
$CMD 100%x93% -resize 1024x768!  5-1-LaunchImage-iOS7-ipad-Landscape.png
$CMD 100%x93% -resize 2048x1536! 5-2-LaunchImage-iOS7-ipad-Landscape@2x.png
$CMD 60%x100% -resize 768x1024!  5-3-LaunchImage-iOS7-ipad-Portrait.png
$CMD 60%x100% -resize 1536x2048! 5-4-LaunchImage-iOS7-ipad-Portrait@2x.png

echo
echo "done."
exit

