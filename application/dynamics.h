/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef DYNAMICS_H
#define DYNAMICS_H

#include <QObject>
#include <QVariant>
#include <QVector>

//=============================================================================
//                             Dynamics Monitor
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \class Dynamics
/// \brief This module measures the loudness of the choire. If activated, it
/// display the corresponding symbols (ppp,pp,p,mp,mf,f,ff,fff) on the screnn.
/// Forte and piano in music is a relative concept. Moreover, it depends on the
/// microphone of the device. Therefore, the module keeps track of the
/// measured loudnesses, storing them in a histogram and using this histogram
/// to decide which levels correspond to ppp and fff.
///////////////////////////////////////////////////////////////////////////////

class Dynamics : public QObject
{
    Q_OBJECT
public:
    explicit Dynamics(QObject *parent = nullptr);

signals:
    void signalDynamicalLevel (QVariant n);

public slots:
    void getVolume(QVariant volume, QVariant adjustedVolume);

private:
    void determineDynamicalRange();
    void computeDynamicalLevel(double logvol);
    void emitDynamicalLevel(int level);

private:
    QVector<int> mHistogram;    // Histogram
    int mCounter;               // Total entries in the histogram
    QVector<double> mAverage;   // Average volume
    int ppp,fff;                // Boundaries of the dynamical range in the histogram
};

#endif // DYNAMICS_H
