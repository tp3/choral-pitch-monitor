/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//============================================================================
//                               Application
//============================================================================

#include <QDebug>
#include <QQuickWindow>
#include <QQmlProperty>
#include <QLibraryInfo>

#include "config.h"
#include "application.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of the Application
/// \details The constructor creates all submodules. It also creates a simple
/// debugging system based on Qt.
/// ///////////////////////////////////////////////////////////////////////////

Application::Application (QGuiApplication &app)
    : QObject()
    , mAppStateHandler(app)
    , mSettings()
    , pEngine(nullptr)
    , mAudioInput()
    , mAnalyzer(mAudioInput)
    , mNoiseDetector(mAudioInput)
    , mDynamics()
{
    // The application uses the in-built error / debug message system of Qt
    // Here we adjust the output format of error messages:
    #ifdef QT_DEBUG
    // In the debug mode verbose messages are shown including file and line number
    qSetMessagePattern("\n%{time h:mm:ss.zzz} \033[0m%{if-debug}Debug%{endif}%{if-info}Info%{endif}%{if-warning}WARNING%{endif}%{if-critical}CRITICAL%{endif}%{if-fatal}FATAL%{endif}: %{message}\n             \033[036m[%{function}] [%{file}:%{line}] [thread=%{threadid}]\033[0m");
    #else
    // In the release mode only short messages are shown in case of warnings, errors, and fatals
    qSetMessagePattern("\033[036m%{if-warning}WARNING: %{message}%{endif}%{if-critical}CRITICAL: %{message}%{endif}%{if-fatal}FATAL: %{message}%{endif}");
    #endif
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Initialization of the Application
/// \details The initialization procedure estableshes all signal-slot
/// connections and calls the initialization of the submodules.
/// \param engine : Reference to the QML-Application engine
///////////////////////////////////////////////////////////////////////////////

void Application::init (QQmlApplicationEngine &engine)
{
    pEngine = &engine;
    connectSignals();
    mAudioInput.init(3000); // Cyclic audio buffer time 3 secs

    // Move Analyzer and NoiseDetector to mThread
    mAnalyzer.moveToThread(&mThread);
    mNoiseDetector.moveToThread(&mThread);
    mDynamics.moveToThread(&mThread);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Start the application
/// \details Start the audio input device and the analyzing components
/// in a separate thread.
///////////////////////////////////////////////////////////////////////////////

void Application::start()
{
    qInfo() << "Starting/resuming application";
    // Get the device name from Qml
    if (not pEngine) return;
    QObject *mainwindow = pEngine->rootObjects().value(0);
    if (not mainwindow) { qCritical() << "Cannot get root object"; return; }
    QString audioDeviceName = mainwindow->property("inputDeviceName").toString();

    // Try to start the audio device used last time
    if (not mAudioInput.start(audioDeviceName)) { qWarning() << "Cannot start audio device"; return; }
    mainwindow->setProperty("inputDeviceName",mAudioInput.getDeviceName());


    // Start thread
    mThread.start();
    if (not mThread.isRunning()) qCritical() << "Cannot start new thread, running application in main thread";
    emit startModules();
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Stop the application
///////////////////////////////////////////////////////////////////////////////

void Application::stop()
{
    qInfo() << "Stopping/suspending application";
    emit stopModules();
    QThread::msleep(100);
    mThread.quit();
    if (not mThread.wait(1000)) qCritical() << "Impossible to quit auxiliary thread";
    mAudioInput.stop();
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Connect signals and slots
///////////////////////////////////////////////////////////////////////////////

void Application::connectSignals()
{
    // Determine the main QML window
    QObject *mainwindow = pEngine->rootObjects().value(0);
    QQuickWindow *qml = qobject_cast<QQuickWindow *>(mainwindow);

    // Application suspend and resume
    connect (&mAppStateHandler,&AppStateHandler::suspend,this,&Application::stop);
    connect (&mAppStateHandler,&AppStateHandler::resume,this,&Application::start);
    connect (this,&Application::startModules,&mAnalyzer,&Analyzer::start);
    connect (this,&Application::startModules,&mNoiseDetector,&NoiseDetector::start);
    connect (this,&Application::stopModules,&mAnalyzer,&Analyzer::stop);
    connect (this,&Application::stopModules,&mNoiseDetector,&NoiseDetector::stop);

    // C++ -> QML
    connect (this,SIGNAL(showError(QVariant)),qml,SLOT(showError(QVariant)));
    connect (&mAudioInput,SIGNAL(showError(QVariant)),qml,SLOT(showError(QVariant)));
    connect (&mAudioInput,SIGNAL(volume(QVariant,QVariant)),qml,SLOT(slotDisplayVolume(QVariant,QVariant)));
    connect (&mAudioInput,SIGNAL(currentlyAvailableInputDevices(QVariant,QVariant)),qml,SLOT(slotUpdateDeviceList(QVariant,QVariant)));
    connect (&mAnalyzer,SIGNAL(signalRequestSettings()), qml, SLOT(slotRequestSettings()));
    connect (&mAnalyzer,SIGNAL(showAlert(QVariant)),qml,SLOT(showAlert(QVariant)));
    connect (&mAnalyzer,SIGNAL(showError(QVariant)),qml,SLOT(showError(QVariant)));
    connect (&mAnalyzer,SIGNAL(signalSetReferenceFrequency(QVariant)), qml, SLOT(slotSetReferenceFrequency(QVariant)));
    connect (&mAnalyzer,SIGNAL(signalGauge(QVariant)), qml, SLOT(slotGauge(QVariant)));
    connect (&mNoiseDetector,SIGNAL(signalEnable(QVariant)), qml, SLOT(slotgaugeActive(QVariant)));
    connect (&mDynamics,SIGNAL(signalDynamicalLevel(QVariant)), qml, SLOT(slotSetDynamicalLevel(QVariant)));

    // QML -> C++
    connect (qml,SIGNAL(requestInputDeviceList()), &mAudioInput, SLOT(requestInputDeviceList()));
    connect (qml,SIGNAL(connectAudioDevice(QString)),&mAudioInput,SLOT(start(QString)));
    connect (qml,SIGNAL(setReferenceFrequency(double)), &mAnalyzer, SLOT(setReferenceFrequency(double)));
    connect (qml,SIGNAL(measureReferenceFrequency()), &mAnalyzer, SLOT(measureReferenceFrequency()));
    connect (qml,SIGNAL(setNoiseDetectorParameters(bool,double)), &mNoiseDetector, SLOT(setNoiseDetectorParameters(bool,double)));
    connect (qml,SIGNAL(setAgility(double)), &mAnalyzer, SLOT(setAgility(double)));
    connect (qml,SIGNAL(resetGauge()), &mAnalyzer, SLOT(resetGauge()));

    // C++ -> C++
    connect (&mNoiseDetector, &NoiseDetector::signalEnable, &mAnalyzer, &Analyzer::enable);
    connect (&mAudioInput,SIGNAL(volume(QVariant,QVariant)),&mDynamics,SLOT(getVolume(QVariant,QVariant)));
}

