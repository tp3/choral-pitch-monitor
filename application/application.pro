#==============================================================================
#                  Choral Pitch Monitor - Application Project File
#==============================================================================

QT += quick multimedia
CONFIG += c++11
TARGET = pitchmonitor

# Platform-dependent notes:
# Add make-install for Windows-UWP

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Include the other modules of the project
include(modules/modules.pri)
include(platformtools/platformtools.pri)

SOURCES += main.cpp \
    application.cpp \
    analyzer.cpp \
    translator.cpp \
    noisedetector.cpp \
    appstatehandler.cpp \
    dynamics.cpp

HEADERS += \
    application.h \
    analyzer.h \
    config.h \
    translator.h \
    noisedetector.h \
    shared.h \
    appstatehandler.h \
    dynamics.h

RESOURCES += resources/qml/qml.qrc \
    resources/icons/icons.qrc \
    resources/images/images.qrc \
    resources/fonts/fonts.qrc \
    resources/translations/languages.qrc

# Include FFTW3 library
DEPENDPATH  += $$OUT_PWD/../thirdparty/fftw3
include(../thirdparty/fftw3/fftw3.pri)

#=============================== Translations =================================

TRANSLATIONS = \
    resources/translations/$${TARGET}_en.ts \ # default language
    resources/translations/$${TARGET}_de.ts \
    resources/translations/$${TARGET}_fr.ts \
    resources/translations/$${TARGET}_es.ts \
    resources/translations/$${TARGET}_it.ts \
    resources/translations/$${TARGET}_ru.ts \
    resources/translations/$${TARGET}_pl.ts \
    resources/translations/$${TARGET}_ro.ts \
    resources/translations/$${TARGET}_zh.ts \
    resources/translations/$${TARGET}_ja.ts \
    resources/translations/$${TARGET}_ko.ts \

# Include QML files as sources:
lupdate_only { SOURCES += resources/qml/*.qml }

#============================== Platform-specific =============================

#------------------------------- Windows UWP ----------------------------------

winrt {
# Copy fftw3.dll to the package. Requires make-install in the projects settings
# ********************* Sometimes this does not work **************************
myinstalls.files = $$OUT_PWD/../thirdparty/fftw3/fftw3.dll
CONFIG(debug, debug|release): myinstalls.path = $$OUT_PWD/debug
CONFIG(release, debug|release): myinstalls.path = $$OUT_PWD/release
INSTALLS += myinstalls
}

#---------------------------------- Linux -------------------------------------

linux:!android {
    QMAKE_CXXFLAGS_DEBUG += -Wall -Werror -Wpedantic

    # set RPATH so that ldd can find the library fttw3
    QMAKE_LFLAGS += "-Wl,-rpath,\'\$$ORIGIN/../thirdparty/fftw3\'"

}

#---------------------------------- MAC-OSX -----------------------------------

mac {
    # If SDK is newer than Qt, you may disable the warnings:
    CONFIG += sdk_no_version_check

    #QMAKE_MAC_SDK=macosx10.13
}

#------------------------------------ IOS -------------------------------------

ios {
    # If SDK is newer than Qt, you may disable the warnings:
    CONFIG += sdk_no_version_check

    # If the wrong bundle identfier (1.pitchmonitor) appears, here is a way out:
    # see https://bugreports.qt.io/browse/QTBUG-66462
#    xcode_product_bundle_identifier_setting.value = "org.pitchmonitor.app"
#    PRODUCT_BUNDLE_IDENTIFIER = "a.b.c"

    # Add the information property list file
    QMAKE_INFO_PLIST = $$PWD/platforms/ios/Info.plist

    # Add asset catalogs (produced by XCode)
    QMAKE_ASSET_CATALOGS = $$PWD/platforms/ios/Images.xcassets
    QMAKE_ASSET_CATALOGS_APP_ICON = "AppIcon"
}

#---------------------------------- Android -----------------------------------


android: {
    # If Android tries to deploy with the SDK build tools of an old version
    # check the file grade.properties and make sure that it is excluded in .gitignore

    QT += androidextras

    DISTFILES += \
        platforms/android/AndroidManifest.xml \
        platforms/android/gradle/wrapper/gradle-wrapper.jar \
        platforms/android/gradlew \
        platforms/android/res/values/libs.xml \
        platforms/android/build.gradle \
        platforms/android/gradle/wrapper/gradle-wrapper.properties \
        platforms/android/gradlew.bat

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/platforms/android
}


#=================================== Misc =====================================

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    platforms/android/AndroidManifest.xml \
    platforms/android/gradle/wrapper/gradle-wrapper.jar \
    platforms/android/gradlew \
    platforms/android/res/values/libs.xml \
    platforms/android/build.gradle \
    platforms/android/gradle/wrapper/gradle-wrapper.properties \
    platforms/android/gradlew.bat \
    platforms/android/AndroidManifest.xml \
    platforms/android/gradle/wrapper/gradle-wrapper.jar \
    platforms/android/gradlew \
    platforms/android/res/values/libs.xml \
    platforms/android/build.gradle \
    platforms/android/gradle/wrapper/gradle-wrapper.properties \
    platforms/android/gradlew.bat

