/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Dynamics Monitor
//=============================================================================

#include "dynamics.h"

#include <QDebug>
#include <QString>

#include <cmath>

#include <QFile>
#include <QTextStream>


///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
///////////////////////////////////////////////////////////////////////////////

Dynamics::Dynamics(QObject *parent)
    : QObject(parent)
    , mHistogram(1024,0)    // The histogram of measured intensities
    , mCounter(-20)         // Skip the first 20 frames
    , mAverage(70,0)        // Use 70 frames for averaging
    , ppp(0)                // ppp-level
    , fff(0)                // fff -level
{}


///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: get volume level
/// \details This slot processes one loudness level emitted by the
/// audio input module. Typicylla these signals arrive 10 times per second.
/// \param volume : Actual volume of the icoming audio signal
/// \param adjustedVolume : Slowly adjusted volume, ignored here
///////////////////////////////////////////////////////////////////////////////

void Dynamics::getVolume(QVariant volume, QVariant adjustedVolume)
{
    if (mCounter<0) { mCounter++; return; } // Skipt first few messages
    (void)adjustedVolume;                   // this parameter is not used
    double vol = volume.toReal();
    if (vol > 0  and vol <= 1)
    {
        double logVolume = -10 * log(vol);
        int index = static_cast<int>(logVolume);
        if (index<0 or index>=mHistogram.size()) return;
        mHistogram[index]++;
        mCounter++;
        if (mCounter % 50 == 0) determineDynamicalRange();

        static int averageIndex = 0;
        mAverage[averageIndex] = logVolume;
        averageIndex = (averageIndex+1) % mAverage.size();

        if (mCounter % 10 == 0)
        {
            double average = 0;
            for (double e : mAverage) average += e;
            computeDynamicalLevel(average/mAverage.size());
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Determine the dynamical range
/// \details Analyzing the current histogram, this routine computes the
/// boundaries (ppp,fff) of the dynamical range.
///////////////////////////////////////////////////////////////////////////////

void Dynamics::determineDynamicalRange()
{
    // we first determine the slot separating the loudest 25 % of the histogram
    int index=0, cumulative=0;
    for (index=0; cumulative<mCounter/4 and index<mHistogram.size(); ++index) cumulative += mHistogram[index];

    // Then we determine the center of mass in a certain window
    int window = std::min(index+45, mHistogram.size());
    double sum=0, norm=0;
    for (int i=0; i<window; ++i) { double weight = sqrt(mHistogram[i]); norm += weight; sum += i*weight; }
    if (norm<=0) return;
    int center = static_cast<int>(sum/norm);
    fff = center-17;
    ppp = center+23;

//    QFile file("/home/hinrichsen/test.dat");
//    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
//              return;
//    QTextStream os(&file);
//    for (int e:mHistogram) os << (double)e/mCounter << endl;
//    file.close();
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Compute the dynamical level between 0 and 7
/// \details This snippet converts the logarithmic volume into an integer
/// number between 0 (ppp) and 7 (fff) depending on the previously determined
/// dynamical range.
///////////////////////////////////////////////////////////////////////////////

void Dynamics::computeDynamicalLevel(double logrithmicVolume)
{
    if (fff==ppp) return;
    int level = static_cast<int>(8*(logrithmicVolume-ppp)/(fff-ppp) - 0.5);
    if (level<0) level=0; else if (level>7) level=7;
    emitDynamicalLevel(level);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Send dynamical level as a string to the GUI
/// \details This function filters fluctuations and listens for changes
/// of the dynamical level. If the level changes, then it is converted into
/// the corresponding string and sent to the QML layer for display.
///////////////////////////////////////////////////////////////////////////////

void Dynamics::emitDynamicalLevel (int level)
{
    const QStringList symbol = {"ppp","pp","p","mp","mf","f","ff","fff"};
    static int previousLevel = -1;
    static int counter = 0;
    if (level != previousLevel)
    {
        counter++;
        if (counter>4*abs(level-previousLevel))
        {
            qDebug() << symbol[level] << mCounter;
            emit signalDynamicalLevel(symbol[level]);
            previousLevel = level;
            counter = 0;
        }
    }
    else counter=0;
}
