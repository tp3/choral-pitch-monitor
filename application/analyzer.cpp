/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Class Analyzer
//=============================================================================

#include "analyzer.h"
#include <QDebug>
#include <QtConcurrent/QtConcurrent>

// A good example of a sliding intonation is:
// https://www.youtube.com/watch?v=Pr9B3ipwMx8

// This generates a standard sine wave 440Hz:
// play -n synth 1000 sin 440

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of the Analyzer
/// \param audioInput : Reference to the audio input module
/// \details The constructor creates and initializes the member variables
///////////////////////////////////////////////////////////////////////////////

Analyzer::Analyzer(AudioInput &audioInput)
    : QObject()
    , pTimer(nullptr)
    , rAudioInput(audioInput)
    , mSampleSize(0)
    , mAudioBuffer()
    , mEnvelope()
    , mFFT()
    , mFFTBuffer()
    , mFilter()
    , mKey()
    , mReferencePhase(1)
    , mResetPhaseTrackingFlag(false)
    , mNormalizedPhases(40)
    , mEnabled(false)
    , mAdiabaticGauge(1)
    , mAgility(0)
{
    mNormalizedPhases.fill(1);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Start the analyzer.
/// \details Calling this function starts the timer and launches the analyzer.
///////////////////////////////////////////////////////////////////////////////

void Analyzer::start()
{
    qInfo() << "Starting analyzer";
    if (not pTimer)
    {
        pTimer = new QTimer(this);
        connect(pTimer,&QTimer::timeout,this,&Analyzer::timeout);
    }
    pTimer->start(updateInterval);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Stop the analyzer
///////////////////////////////////////////////////////////////////////////////

void Analyzer::stop()
{
    qInfo() << "Stopping analyzer";
    pTimer->stop();
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Enable circular gauge
///////////////////////////////////////////////////////////////////////////////

void Analyzer::enable (QVariant on)
{
    mEnabled = on.toBool();
    if (not mEnabled) mResetPhaseTrackingFlag = true;
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Measure the reference frequency.
/// \details The application has essentially a single button. When pressed,
/// the application listens to the music and defines the measured pitch as
/// the reference pitch (0 cents). To this end the recent phase factors are
/// continuously stored in an array. This function averages over the values
/// actually contained in the array and computes the corresponding phase
/// factor (stored in mReferencePhase) and the corresponding frequency in Hz.
/// The result is submitted as a signal signalSetReferenceFrequency.
///////////////////////////////////////////////////////////////////////////////

void Analyzer::measureReferenceFrequency()
{
    complex sum=0;
    for (complex &c : mNormalizedPhases) sum+=c;
    if (abs(sum)<1E-100) return;
    mReferencePhase = sum/abs(sum);
    double phase = arg(sum)/M_PI;
    double f = f0*pow(2,1/24.0*phase);
    qInfo() << "New measured reference pitch: " << f;
    mResetPhaseTrackingFlag = true;
    signalSetReferenceFrequency(static_cast<int>(0.5+f));
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set the reference frequency
/// \details This slot allows QML to set the modified or stored reference
/// frequency
/// \param f : Reference frequency in Hz
///////////////////////////////////////////////////////////////////////////////

void Analyzer::setReferenceFrequency (double f)
{
    mReferencePhase = exp(complex(0,24*M_PI*log(f/f0)/log(2)));

    // Rechne rückwärts, das kann man nachher entfernen und direkt f senden
    double phase = arg(mReferencePhase)/M_PI;
    double fc = f0*pow(2,1/24.0*phase);
    qInfo() << "New specified reference pitch: " << f;
    resetGauge();
    signalSetReferenceFrequency(static_cast<int>(0.5+fc));
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set the reference frequency
///////////////////////////////////////////////////////////////////////////////

void Analyzer::setAgility(double agility)
{
    mAgility = agility;
    qInfo() << "Agility =" << agility;
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Reset circular gauge
///////////////////////////////////////////////////////////////////////////////

void Analyzer::resetGauge()
{
    mResetPhaseTrackingFlag = true;     // reset tracking
    mAdiabaticGauge = mReferencePhase;  // put gauge to neutral position
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Private initialization function
/// \param msec: Size of the time window to be evaluated.
/// \details This function initializes the analyzer at the beginning and
/// whenever the sample rate of the audio unit changes.
/// return : true if initialized
///////////////////////////////////////////////////////////////////////////////

bool Analyzer::init (int msec)
{
    mSampleRate = rAudioInput.getSampleRate();
    const int sampleSize = mSampleRate * msec / 1000;
    const int cyclicBufferSize = rAudioInput.getBufferSize();
    if (cyclicBufferSize==0 or sampleSize==0 or
        sampleSize>cyclicBufferSize) return false;

    // Return if already initialized
    if (sampleSize == mSampleSize) return true;

    // Resize buffer
    mAudioBuffer.resize(sampleSize);

    // Store new sample size
    mSampleSize = sampleSize;
    const int FFTSize = sampleSize/2+1;

    // Compute frequency filter function
    mFilter.resize(FFTSize);
    const double lowerCutoff = 300;
    const double upperCutoff = 1100;
    const complex alpha = complex(0,24*M_PI/log(2));
    mFilter[0]=0;
    for (int i=1; i<mFilter.size(); ++i)
    {
        double f = sampleSize*static_cast<double>(i)/mSampleRate;
        double val = pow(f/upperCutoff,10) + pow(lowerCutoff/f,4);
        double amp = (val > 10000 ? 0 : exp(-val));
        mFilter[i] = amp * pow(complex(f/f0,0),alpha);
    }

    // Compute evenvelope
    mEnvelope.resize(sampleSize/100);
    for (int i=0; i<mEnvelope.size(); ++i) mEnvelope[i] = pow(sin(M_PI*i/mEnvelope.size()/2.0),2);

    // Optimize the slow FFT immediately
    mFFT.optimize(mAudioBuffer,FFTW_MEASURE);

    // Tell Qml to send the actual settings
    emit signalRequestSettings();
    return true;
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Timeout function, carrying out the main analysis
/// \details This function, triggered by the timer every say 100 ms, does the
/// main work. First it prevents multiple execution in case that the timer
/// triggers faster than the CPU can perform.
///////////////////////////////////////////////////////////////////////////////

void Analyzer::timeout()
{
    // Prevent multiple execution
    static QMutex preventMultipleExecution;
    if (not preventMultipleExecution.try_lock()) { qWarning() << "Timing problem"; return; }
    else preventMultipleExecution.unlock();
    QMutexLocker locker(&preventMultipleExecution);

    // Initialize if necessary, audio buffer = 1000 msec
    if (not init(1000)) return;

    // Get complete audio buffer
    rAudioInput.copyAudioBuffer(mAudioBuffer);

    // Mollify data with on-off envelope at the ends of the sample
    if (mEnvelope.size() < mAudioBuffer.size()/2)
        for (int i=0; i<mEnvelope.size(); ++i)
        {
            mAudioBuffer[i]*=mEnvelope[i];
            mAudioBuffer[mAudioBuffer.size()-1-i]*=mEnvelope[i];
        }

    // Compute FFT
    mFFT.calculateFFT(mAudioBuffer,mFFTBuffer);


    // Evaluate the complex pointer and the total power
    complex measuredPhase=0;
    double totalPower=0;
    for (int j=0; j<mFFTBuffer.size(); ++j)
    {
        double power = (mFFTBuffer[j]*conj(mFFTBuffer[j])).real();
        measuredPhase += power * mFilter[j];
        totalPower += power * abs(mFilter[j]);
    }

    // Check whether the data is useful
    double significance = abs(measuredPhase)/totalPower;
    bool dataIsUseful = (significance > 0.14);

    //qDebug() << arg(measuredPhase);

    // Store normalized and unnormalized phases in memory
    static int historyPointer = 0;
    mNormalizedPhases[(historyPointer)%mNormalizedPhases.size()] =
         (dataIsUseful and totalPower > 0 ? measuredPhase/totalPower : 0);
    historyPointer++;

    static double maxGauge=0.01;
    if (mEnabled)
    {
        double prefactor = 0.4 * pow(mAgility,2);
        // If the phase is opposite we decrease the amplitude substantially
        if ((mAdiabaticGauge/measuredPhase).real()<0) prefactor /= 20;
        mAdiabaticGauge += prefactor * (measuredPhase-mAdiabaticGauge);
        if (abs(mAdiabaticGauge)>maxGauge) maxGauge=abs(mAdiabaticGauge);
    }
    // Otherwise let the gauge run into the neutral position after some time
    else  mAdiabaticGauge +=0.05*(mReferencePhase-mAdiabaticGauge);
    setGauge(mAdiabaticGauge);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Function communicating with the QML layer to set the circular gauge.
/// \param phase : Actual complex number carrying the phase, zero if inactive
///////////////////////////////////////////////////////////////////////////////

void Analyzer::setGauge (Analyzer::complex phase)
{
    // Valid inputs are nonzero
    bool valid = (abs(phase) > 1E-20);

    // Avoid overturning gauge when touching the limits
    static complex previousPhase = 1;
    static double centDeviation = 0;
    if (valid)
    {
        double phaseDifference = arg(phase/previousPhase);
        if (mResetPhaseTrackingFlag or (fabs(phaseDifference)>0.2))
        {
            centDeviation = 50.0 * arg(phase/mReferencePhase)/M_PI;
            //qDebug() << "Setting" << centDeviation;
        }
        else
        {
            centDeviation += 50.0 * phaseDifference /M_PI;
            //qDebug() << "Tracing" << centDeviation;
        }
        previousPhase = phase;
        mResetPhaseTrackingFlag = (fabs(centDeviation)>70);
        //qDebug() << centDeviation;
        emit signalGauge(centDeviation);
    }

    // Handle intonation alarm indicators (arrows up and down)
    // The intonation alarm is enabled only as long as the function receives
    // sufficiently many valid (=nonzero) values complex values
    static bool intonationAlarmEnabled = true;
    static bool intonationAlarmActivated = false;
    static int counter = 0; const int counterMax = 10;
    if (valid) { if (counter>0) counter--; }
    else       { if (counter<counterMax) counter++; }
    if (counter==0) intonationAlarmEnabled = false;
    if (counter==counterMax) intonationAlarmEnabled = true;

    bool activate = (intonationAlarmEnabled and fabs(centDeviation) < 30);
    if (activate != intonationAlarmActivated)
    {
        intonationAlarmActivated = activate;
        signalIntonationAlarm(activate);
    }
}

