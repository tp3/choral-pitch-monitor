/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//============================================================================
//                               Application
//============================================================================

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QThread>
#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QSettings>

#include "audioinput.h"
#include "analyzer.h"
#include "translator.h"
#include "appstatehandler.h"
#include "dynamics.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Application class
/// \details This class manages the application. It initializes, starts and
/// stops the moduls and establishes the interface with the QML layer.
///////////////////////////////////////////////////////////////////////////////

class Application : public QObject
{
    Q_OBJECT

public:
    Application (QGuiApplication &gui);

    void init (QQmlApplicationEngine &engine);
    void exit() {}

public slots:
    void start();
    void stop();

private:
    void connectSignals();

signals:
    void startModules();
    void stopModules();
    void showError(QVariant);
    void setQmlLocale(QVariant string);
    void requestAudioConnection();

private:
    AppStateHandler mAppStateHandler;   ///< Application state handler
    QSettings mSettings;                ///< Language settings

    QQmlApplicationEngine *pEngine;     ///< Pointer to the applicaiton engine
    AudioInput mAudioInput;             ///< Instance of audio input
    Analyzer mAnalyzer;                 ///< Instance of the analyzer
    QThread mThread;                    ///< Separate thread for the analyzer
    NoiseDetector mNoiseDetector;       ///< Detector module for noise
    Dynamics mDynamics;                 ///< Loudness monitor
};

#endif // APPLICATION_H
