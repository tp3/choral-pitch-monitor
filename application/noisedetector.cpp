/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include "noisedetector.h"
#include <QDebug>

//=============================================================================
//                             Class NoiseDetector
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor, initializing the NoiseDetector
/// \param audioInput : Audio input module
///////////////////////////////////////////////////////////////////////////////

NoiseDetector::NoiseDetector(AudioInput &audioInput)
    : pDetectorTimer(nullptr)
    , rAudioInput(audioInput)
    , mSampleRate(0)
    , mAudioBufferSize(0)
    , mFFT()
    , mSample()
    , mSpectrum()
    , mKmin(0), mKmax(0)
    , mCyclicSpectrum()
    , mOctaveSpectrum()
    , mPreviousSpectrum()
    , mCyclicLogMap()
    , mOctaveLogMap()
    , mInitialized(false)
    , mNoiseThreshold(0.3)
    , mPreviousTotalPower(0)
{
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Initialize the noise detector
///////////////////////////////////////////////////////////////////////////////

void NoiseDetector::init()
{
    mInitialized = false;

    // Frequency range of noise detection
    const int fmin=100, fmax=10000;

    // Sample size in milliseconds
    const int msec = 500;

    // Number of bins
    const int cyclicBins = 6;  // bins per half tone, cyclically mapped
    const int octaveBins = 240;  // bins per octave for correlation measurement

    // Get audio parameters
    const int sr = rAudioInput.getSampleRate();
    const int bs = rAudioInput.getBufferSize();

    // Require at least 1sec of buffer size
    if (bs < sr) { qCritical() << "Circular buffer size" << bs << "smaller than sample rate" << sr; return; }

    if (sr != mSampleRate or bs != mAudioBufferSize)
    {
        mSampleRate = sr;
        mAudioBufferSize = bs;
        const int ss = (sr*msec)/1000;
        mSample.resize(ss);
        mFFT.optimize(mSample,FFTW_MEASURE);

        // Fourier modes to be evaluated
        mKmin = (fmin*ss)/sr;
        mKmax = (fmax*ss)/sr;
        if (mKmin<1 or mKmax>ss) { qCritical() << "Internal error kmin =" << mKmin << "and Kmax =" << mKmax; return; }

        // Half tone analysis initialization
        mCyclicSpectrum.resize(cyclicBins);
        mOctaveSpectrum.resize(octaveBins);
        mPreviousSpectrum.resize(octaveBins);
        mOctaveSpectrum.fill(0);
        mCyclicSpectrum.fill(0);
        mCyclicLogMap.resize(mKmax);
        mOctaveLogMap.resize(mKmax);
        for (int k=1; k<mKmax; k++)
        {
            double log2k = log(static_cast<double>(k)) / log(2);
            mCyclicLogMap[k] = (static_cast<int>(12 * cyclicBins * log2k)) % cyclicBins;
            mOctaveLogMap[k] = (static_cast<int>(octaveBins * log2k)) % octaveBins;
        }
    }
    mInitialized = true;
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Start the noise detector
/// \details This function starts the timer of the NoiseDetector which
/// triggers the function detect() every 500 ms.
///////////////////////////////////////////////////////////////////////////////

void NoiseDetector::start()
{
    qInfo() << "Starting noise-detector";
    if (not pDetectorTimer)
    {
        pDetectorTimer = new QTimer(this);
        connect(pDetectorTimer,&QTimer::timeout,this,&NoiseDetector::detect);
    }
    pDetectorTimer->start(500);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Stop the noise detector
///////////////////////////////////////////////////////////////////////////////

void NoiseDetector::stop()
{
    qInfo() << "Stopping noise detector";
    pDetectorTimer->stop();
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set the parameters of the noise detector
/// \param noiseDetectionEnabled : Enable / disable noise detector
/// \param noiseThreshold : Actual sensitivity of the noise detector
///////////////////////////////////////////////////////////////////////////////

void NoiseDetector::setNoiseDetectorParameters(bool noiseDetectionEnabled, double noiseThreshold)
{
    if (fabs(noiseThreshold-mNoiseThreshold)>1E-100)
    {
        qInfo() << "New noise threshold =" << mNoiseThreshold;
        mNoiseThreshold = noiseThreshold;
    }
    if (noiseDetectionEnabled != pDetectorTimer->isActive())
    {
        qInfo() << "Noise detector active =" << noiseDetectionEnabled;
        if (noiseDetectionEnabled) pDetectorTimer->start();
        else
        {
            setOperationMode(on,"Permanently on");
            pDetectorTimer->stop();
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Set the operation mode of the circular gauge (on or off)
/// \details This function emits the signalOperationMode(bool) when turning
/// the circular gauge on and off. In the frontent this state is indicated
/// by a red lamp just below the axis of the circular gauge.
/// \param s : on or off
/// \param msg: Message to be displayed by qInfo()
///////////////////////////////////////////////////////////////////////////////

void NoiseDetector::setOperationMode(NoiseDetector::state s, QString msg)
{
    static state current = undefined;
    if (s != current)
    {
        current = s;
        if (s==on)          { qInfo() << "ON: " << msg; signalEnable(true); }
        else if (s==off)    { qInfo() << "OFF:" << msg; signalEnable(false); }
    }
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Detect noise and music
/// \details This is the main function of the NoiseDetector. It is called
/// every 500ms by a timer. When triggered, it gets the latest 500ms of
/// audio data from the cyclic buffer in the AudioInput module. This signal
/// is the Fourier-transformed by an FFT. In the spectrum, the half tones are
/// divided into N bins and the spectrum is logarithmically binned in a
/// cyclic manner. After normalizing the spectrum, the variance of the cyclic
/// spectrum is determined. A small variance indicates music (most spectral
/// lines arranged in a grid of half tones) while a large variance indicates
/// noise (all spectral lines equally distributed). Finally, the correlation
/// between the actual and the previous cyclic spectrum is measured. A large
/// deviation indicates a sudden stop of the music, turning the circular
/// gauge off.
///////////////////////////////////////////////////////////////////////////////

void NoiseDetector::detect()
{
    if (not mInitialized) { init(); return; }

    // Get audio PCM data and calculate FFT
    rAudioInput.copyAudioBuffer(mSample);
    mFFT.calculateFFT(mSample,mSpectrum);

    // Remember previous octave spectrum
    mPreviousSpectrum = mOctaveSpectrum;

    // Determine cyclic spectrum
    mCyclicSpectrum.fill(0);
    mOctaveSpectrum.fill(0);
    double totalPower=0;
    for (int k=mKmin; k<mKmax; ++k)
    {
        double power = std::abs(mSpectrum[k]*mSpectrum[k]);
        totalPower += power;
        mCyclicSpectrum[mCyclicLogMap[k]] += power;
        mOctaveSpectrum[mOctaveLogMap[k]] += power;
    }

    // Normalize the cyclic spectrum
    if (totalPower <= 1E-100) { setOperationMode(off,"No input signal"); return; }
    if (totalPower <= mPreviousTotalPower*0.01) { setOperationMode(off,"Sudden drop in volume"); return; }
    mPreviousTotalPower = totalPower;
    for (double &e : mCyclicSpectrum) e /= totalPower;
    for (double &e : mOctaveSpectrum) e /= totalPower;

    //
    complex actual=0, previous=0, correlation=0;
    for (int i=0; i<mOctaveSpectrum.size(); i++)
    {
        complex phase = exp(complex(0,2*3.14159265358979323846*i/20));
        actual      +=  phase * mOctaveSpectrum[i];
        previous    +=  phase * mPreviousSpectrum[i];
        correlation +=  phase * mOctaveSpectrum[i]*mPreviousSpectrum[i];
    }

    // Take action and switch the gauge on and off
    double f= exp(3*(mNoiseThreshold-0.5));
    if (abs(actual*actual)<f*0.005 and abs(correlation)<f*0.001) setOperationMode(off,"off");
    if (abs(actual*actual)>f*0.05  and abs(previous*previous)>f*0.05 and abs(correlation)>f*0.01) setOperationMode(on,"ON");
}





