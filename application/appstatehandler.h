/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                          Application State Handler
//=============================================================================

#ifndef APPSTATEHANDLER_H
#define APPSTATEHANDLER_H

#include <QGuiApplication>

class AppStateHandler : public QObject
{
    Q_OBJECT
public:
    explicit AppStateHandler (QGuiApplication &app);

private slots:
    void applicationStateChanged(Qt::ApplicationState);

signals:
    void resume();
    void suspend();

private:
    Qt::ApplicationState previousState;
};

#endif // APPSTATEHANDLER_H
