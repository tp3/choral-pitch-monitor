/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Class AudioInput
//=============================================================================

#ifndef AUDIOINPUT_H
#define AUDIOINPUT_H

#include <QAudioInput>
#include <QBuffer>
#include <QVector>
#include <QMutexLocker>
#include <QVariant>

#include "shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Class for platform-independent audio input based on Qt
///////////////////////////////////////////////////////////////////////////////

class AudioInput : public QObject
{
    Q_OBJECT
//--------------------------------------------------------

public:
    AudioInput();
    bool init (const int milliseconds);
    bool exit();

    const QString& getDeviceName() const { return mDeviceName; }
    int  getBufferSize() const { return mCyclicBuffer.size()/2; }
    int  getSampleRate() const { return mAudioFormat.sampleRate(); }

    bool copyAudioBuffer (QVector<double> &buffer) const;

public slots:
    bool start(const QString deviceName = "automatic");
    bool stop();

    void requestInputDeviceList();

signals:
    void showError (QVariant msg);
    void volume (QVariant volume, QVariant adjustedVolume);
    void currentlyAvailableInputDevices (QVariant list, QVariant index);

//    void isConnected (QString device, int sampleRate);
//    void availableDevices (QStringList list);

//--------------------------------------------------------

private:
    bool audioErrorOccured();
    void adaptCyclicBufferSize (int size);
    double autoLevelAdjustment (const double &pcmValue, bool update);

private slots:
    void sendDeviceList();
    void receiveAudioData();

private:
    QString mDeviceName;
    QAudioInput* pAudioInput;
    QAudioFormat mAudioFormat;
    QIODevice* pAudioInputBuffer;
    int mBufferSizeMilliseconds;
    QVector<double> mCyclicBuffer;
    int mWritePosition;
    mutable QMutex mCyclicBufferMutex;
    double mOverdrive, mSlowLevel, mFastLevel;
};

#endif // AUDIOINPUT_H
