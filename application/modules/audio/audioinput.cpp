/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Class AudioInput
//=============================================================================

#include "audioinput.h"

#include <cmath>

#include <QAudioDeviceInfo>
#include <QtEndian>
#include <QDebug>
#include <QTimer>

//-----------------------------------------------------------------------------
//                          AudioInput constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of AudioInput
///////////////////////////////////////////////////////////////////////////////

AudioInput::AudioInput()
    : mDeviceName("")
    , pAudioInput(nullptr)
    , mAudioFormat()
    , pAudioInputBuffer(nullptr)
    , mBufferSizeMilliseconds(1000)
    , mCyclicBuffer()
    , mWritePosition(0)
    , mCyclicBufferMutex()
    , mOverdrive(0)
    , mSlowLevel(0.001)
    , mFastLevel(1)
{
}


//-----------------------------------------------------------------------------
//                          Initialize audio input
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Initialize audio input and set the size of the internal audio buffer
/// \param milliseconds : Maximal recording time of the buffer in milliseconds
///////////////////////////////////////////////////////////////////////////////

bool AudioInput::init (const int milliseconds)
{
    mBufferSizeMilliseconds = milliseconds;
    return true;
}


//-----------------------------------------------------------------------------
//                            Audio input shutdown
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Shut down audio input
///////////////////////////////////////////////////////////////////////////////

bool AudioInput::exit()
{
    if (pAudioInput)
    {
        pAudioInput->stop();
        pAudioInput->deleteLater();
        pAudioInput = nullptr;
    }
    return true;
}



//-----------------------------------------------------------------------------
//                    Public slot: Start audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Start audio device
/// \details This function connects the audio device with a given deviceName
/// which as to be one of the strings given back by availableDevices().
/// The function tries to connect to a device in the nearest format.
/// \param deviceName : Name of the audio input device to be connected,
///                     empty for automatic connection
///////////////////////////////////////////////////////////////////////////////


bool AudioInput::start (const QString deviceName)
{
    if (deviceName.size()==0) return start("automatic");

    // If device is already running quit
    if (pAudioInput and mDeviceName == deviceName)
    {
        qInfo() << "Device is already connected";
        return true;
    }

    // Stop currently running device
    mDeviceName = "";
    if (pAudioInput)
    {
        pAudioInput->stop();
        pAudioInput->deleteLater();
        pAudioInput = nullptr;
    }

    // Set up the desired format:
    mAudioFormat.setSampleRate(44100);
    mAudioFormat.setChannelCount(1);
    mAudioFormat.setSampleSize(16);
    mAudioFormat.setCodec("audio/pcm");
    mAudioFormat.setByteOrder(QAudioFormat::LittleEndian);
    mAudioFormat.setSampleType(QAudioFormat::SignedInt);

    // Find the specified or the default device
    QAudioDeviceInfo deviceInfo;
    if (deviceName=="automatic") deviceInfo = QAudioDeviceInfo::defaultInputDevice();
    else
    {
        QList<QAudioDeviceInfo> list = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
        for (QAudioDeviceInfo &entry : list)
            if (entry.deviceName() == deviceName) { deviceInfo = entry; break; }
        if (deviceInfo.deviceName() != deviceName)
        {
            qWarning() << "Could not find audio device" << deviceName
                       << "- using default device" << deviceInfo.deviceName() << "instead.";
            emit showError(tr("Audio device not found.")+" "+tr("Using default device instead."));
            return start("automatic");
        }
    }
    if (not deviceInfo.isFormatSupported(mAudioFormat))
    {
        qWarning() << "Audio input default format not supported, trying to use the nearest fallback option.";
        mAudioFormat = deviceInfo.nearestFormat(mAudioFormat);
    }
    if (not deviceInfo.isFormatSupported(mAudioFormat))
    {
        qWarning() << "Even the nearest fallback format is not supported.";
        emit showError(tr("Cannot find audio input device."));
        return false;
    }
    qInfo() << "Connecting to" << deviceInfo.deviceName() << mAudioFormat;
    pAudioInput = new QAudioInput(deviceInfo,mAudioFormat);
    if (audioErrorOccured())
    {
        qWarning() << "Error creating audio device";
        if (pAudioInput) pAudioInput->deleteLater();
        pAudioInput = nullptr;
        emit showError(tr("Cannot open audio device."));
        return false;
    }
    qInfo() << "Audio input device" << deviceInfo.deviceName() << "successfully created, now starting...";
    pAudioInputBuffer = pAudioInput->start();
    if (not pAudioInputBuffer)
    {
        qWarning() << "Could not get pointer to AudioInputBuffer";
        if (pAudioInput) pAudioInput->deleteLater();
        pAudioInput = nullptr;
        return false;
    }
    connect(pAudioInputBuffer,&QIODevice::readyRead,this,
            &AudioInput::receiveAudioData);
    mDeviceName = deviceName;
    return true;
}


//-----------------------------------------------------------------------------
//                    Public slot: Stop audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Stop audio device
///////////////////////////////////////////////////////////////////////////////

bool AudioInput::stop()
{
    qInfo() << "Stopping audio input device";
    if (pAudioInputBuffer)
    {
        disconnect(pAudioInputBuffer,&QIODevice::readyRead,this,&AudioInput::receiveAudioData);
        pAudioInput->stop();
    }
    pAudioInputBuffer = nullptr;
    mDeviceName.clear();
    return true;
}


//-----------------------------------------------------------------------------
//                                Get buffer
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Get the buffer content
/// \details This function copies the actual buffer content to the buffer
/// passed as a reference parameter. The last entry of this vector
/// contains the most recent pcm value. The size of the buffer has to be
/// resized before, its size is not changed in this function.
/// \param buffer : Reference to the buffer of pcm values.
/// \return True if successful
///////////////////////////////////////////////////////////////////////////////

bool AudioInput::copyAudioBuffer (QVector<double> &buffer) const
{
    if (not pAudioInput or not pAudioInputBuffer) return false;
    QMutexLocker lock(&mCyclicBufferMutex);
    int maxBufferSize = mCyclicBuffer.size()/2;
    if (buffer.size()==0 or buffer.size()>maxBufferSize)
    {
        qCritical() << "Cannot copy buffer with size" << buffer.size()
                    << "of cyclic buffer with max size" << maxBufferSize;
        buffer.fill(0);
        return false;
    }
    if (mWritePosition<maxBufferSize or mWritePosition>=2*maxBufferSize)
    {
        qCritical() << "mWritePosition range illegal";
        buffer.clear();
        return false;
    }
    const double* source = &mCyclicBuffer[mWritePosition-buffer.size()];
    double* dest = &buffer[0];
    memcpy (dest,source,static_cast<size_t>(buffer.size())*sizeof(double));
    return true;
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Request list auf audio input devices
/// \details : Entering the settings page, the QML layer sends a request
/// to this slot to update the list of currently available audio devices.
/// Since the audio layer is running in the main thread and such a request
/// costs some time, we delay here the request in order to allow QML
/// to complete the animation. The function then calls the private
/// function sendDeviceList().
///////////////////////////////////////////////////////////////////////////////

void AudioInput::requestInputDeviceList()
{
    qInfo() << "Requesting device list";
    QTimer::singleShot(650,this,SLOT(sendDeviceList()));
}


//-----------------------------------------------------------------------------
//                       Check Qt audio error messages
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Check Qt audio error messages
///////////////////////////////////////////////////////////////////////////////

bool AudioInput::audioErrorOccured()
{
    if (not pAudioInput) return true;
    switch (pAudioInput->error())
    {
    case QAudio::NoError:
        return false;
    case QAudio::OpenError:
        qCritical() << "An error occurred opening the audio device";
        return true;
    case QAudio::IOError:
        qCritical() << "An error occurred during read/write of audio device";
        return true;
    case QAudio::UnderrunError:
        qCritical() << "Audio data is not being fed to the audio device at a fast enough rate";
        return true;
    case QAudio::FatalError:
        qCritical() << "A non-recoverable error has occurred, the audio device is not usable at this time";
        return true;
    }
    return false;
}


//-----------------------------------------------------------------------------
//              Adapt the size of the cyclic buffer if necessary
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief AudioInput::resetCyclicBuffer
///////////////////////////////////////////////////////////////////////////////

void AudioInput::adaptCyclicBufferSize(int size)
{
    mCyclicBuffer.resize(size);
    mCyclicBuffer.fill(0);
    mWritePosition = mCyclicBuffer.size()/2;
}


//-----------------------------------------------------------------------------
//                   Private slot: Receive new audio data
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Receive new audio data
///////////////////////////////////////////////////////////////////////////////

void AudioInput::receiveAudioData()
{
    const int sampleRate = mAudioFormat.sampleRate();
    const int framesPerLevelAdjustment = sampleRate / 100; // 10 ms
    const int bufferSize = mBufferSizeMilliseconds*sampleRate/1000;
    if (mCyclicBuffer.size() != 2*bufferSize) adaptCyclicBufferSize(2*bufferSize);

    // Get all available audio data
    const QByteArray data = pAudioInputBuffer->readAll();
    const int channelCount = mAudioFormat.channelCount();
    if (channelCount < 1 or channelCount > 5)
    {
        qCritical() << "Audio input with" << channelCount << " not supported.";
        emit pAudioInput->stop();
        return;
    }
    const int bytesPerFrame = mAudioFormat.bytesPerFrame();
    const int numberOfFrames = data.size() / bytesPerFrame;
    const int bytesPerChannel = bytesPerFrame / channelCount;
    const QAudioFormat::SampleType type = mAudioFormat.sampleType();
    const bool littleEndian = (mAudioFormat.byteOrder() == QAudioFormat::LittleEndian);
    for (int frame=0; frame < numberOfFrames; frame++)
    {
        double pcmMonoValue = 0;
        for (int channel=0; channel < channelCount; channel++)
        {
            int cursor = bytesPerFrame*frame + bytesPerChannel*channel;
            switch (type)
            {
            case QAudioFormat::SignedInt:
                if (bytesPerChannel==1) pcmMonoValue += *((qint8*)&(data.data()[cursor])) / 128.0;
                else if (littleEndian) pcmMonoValue  += *((qint16*)&(data.data()[cursor])) / 32768.0;
                else pcmMonoValue += qToLittleEndian(*((qint16*)&(data.data()[cursor])))  / 32768.0;
                break;
            case QAudioFormat::UnSignedInt:
                if (bytesPerChannel==1) pcmMonoValue += *((quint8*)&(data.data()[cursor])) / 128.0 - 1.0;       // tested
                else if (littleEndian) pcmMonoValue  += *((quint16*)&(data.data()[cursor])) / 32768.0 - 1.0;
                else pcmMonoValue += qToLittleEndian(*((quint16*)&(data.data()[cursor])))  / 32768.0;
                break;
//            case QAudioFormat::Float:
//                qDebug() << "float";
//                break;
            default:
                qCritical() << "Sample type #" << type << " not yet implemented.";
                emit pAudioInput->stop();
                return;
            }
        }        

        // Copy pcm-value to buffer
        double pcmAdjusted = autoLevelAdjustment (pcmMonoValue, mWritePosition % framesPerLevelAdjustment==0);
        mCyclicBufferMutex.lock();
        mCyclicBuffer[mWritePosition] = pcmAdjusted;
        mWritePosition++;
        if (mWritePosition >= 2*bufferSize)
        {
            memcpy(mCyclicBuffer.data(),mCyclicBuffer.data()+bufferSize,sizeof(double)*static_cast<size_t>(bufferSize));
            memset(mCyclicBuffer.data()+bufferSize,0,sizeof(double)*static_cast<size_t>(bufferSize));
            mWritePosition = bufferSize;
        }
        mCyclicBufferMutex.unlock();
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Automatic audio input level adjustment
/// \param pcmValue : Raw unnormalized PCM audio value
/// \param update : Boolean which is true in intervals of 10 ms of calling
/// \return Normalized amplitude
/// \details This function implements an automatic input level control.
/// It also emits the signal 'volume' containing the actual raw level
/// of the signal and the adjusted volume by which the PCM value was divided.
///////////////////////////////////////////////////////////////////////////////

double AudioInput::autoLevelAdjustment (const double &pcmValue, bool update)
{
    static double level = 0;                    // measured level between subsequent updates
    static double maxLevel = 0;                 // maximal level of five measurements
    static double fastAverageLevel = 1E-12;     // Adiabatic follower on a short time scale
    static double factor = 0.3;                 // Inverse time scale for the slow average
    static double slowAverageLevel = 1E-12;     // Adoabatoc fpööpwer on a slow time scale
    static int counter = 0;

    // Find maximal amplitude in approx 10 frames
    double amplitude = fabs(pcmValue);
    if (amplitude > level)
    {
        level = amplitude;
        if (amplitude > fastAverageLevel) fastAverageLevel = amplitude;
    }

    if (update) // update every 10 ms if signal is nonzero
    {
        // clip values that are 100 times larger than actual level
        if (level > slowAverageLevel*100)
            level = fastAverageLevel = slowAverageLevel*100;

        // Compute maxLevel to be shown in VU meter
        if (level > maxLevel) maxLevel = level;
        level = 0;

        // The VU meter gets a signal every 50 ms
        if (counter++ % 5 == 0) // every 50 ms
        {
            if (factor > 0.01) factor += 0.01 * (0.0095-factor);
            if (maxLevel > slowAverageLevel)
                    slowAverageLevel += factor*(maxLevel-slowAverageLevel);
            else    slowAverageLevel += 0.0005*(maxLevel-slowAverageLevel);

            // Send information to VU meter
            emit volume(maxLevel,fastAverageLevel);

            double difference = fastAverageLevel-slowAverageLevel;
            if (difference > 0 and slowAverageLevel > 0)
                fastAverageLevel -= qMin(0.5 * fastAverageLevel,
                                         0.1 * difference*difference/slowAverageLevel);
            maxLevel = 0;
        }
    }
    return (amplitude <= fastAverageLevel ? pcmValue / fastAverageLevel : 0);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Send the actual list of available audio devices
///////////////////////////////////////////////////////////////////////////////

void AudioInput::sendDeviceList()
{
    QList<QAudioDeviceInfo> devInfoList =
            QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
    QStringList deviceList = {tr("Automatic selection")};
    for (QAudioDeviceInfo &info: devInfoList)
    {
        if (info.deviceName().contains("CARD")) continue;
        deviceList += info.deviceName();
    }
    int index = 0; // entry to be highlighted
    if (deviceList.contains(mDeviceName)) index = deviceList.indexOf(mDeviceName);
    emit currentlyAvailableInputDevices (deviceList,index);
}

