/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//           FFTW3 implementation for fast Fourier transformations
//=============================================================================

#ifndef FOURIERTRANSFORM_H
#define FOURIERTRANSFORM_H

#include "../thirdparty/fftw3/fftw3.h"
#include <complex>
#include <QVector>
#include <QMutex>
#include <QDebug>

#include "shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Thread-safe implementation of fftw3
///
/// <b>USAGE:</b> FFT's can be carried out simply by calling the
/// function calculateFFT with the corresponding arguments. If a given
/// instance has to carry out a larger number of FFTs using the same vector
/// sizes it is meaningful to optimize the calculation by initially calling the
/// function optimize with the input vector as the argument.
///
/// <b>DETAILS:</b>
/// A Fourier transformation using fftw3 requires two steps. First a
/// so-called 'plan' has to be computed which optimizes the
/// FFT algorithm. Then in a second step the actual Fourier transform is
/// carried out. A plan may be kept for several calculations as long
/// as the location of the vectors and their sizes are not changed.
/// By keeping a plan the computation time is drastically reduced.
///
/// As of now, the second step (the FFT) is thread-safe while the first one is not.
/// This means that only one thread is allowed to create a plan at a given
/// time. To this end this implementation class protects all accesses
/// to planmaking by a static mutex. In addition, it keeps local input
/// and output vectors with a constant location.
///
/// The class provides two types of FFTs, namely, real to complex
/// and complex to real. The second one is the inverse of the first one.
///
/// Since memory allocation should be carried out with the inbuilt
/// allocation function of FFTW3, the implementation copies the vectors
/// into local member vectors by memcpy.
///////////////////////////////////////////////////////////////////////////////

class FourierTransform
{
public:

    FourierTransform();
    ~FourierTransform();

    using complex = std::complex<double>;

    void calculateFFT  (const QVector<double> &in, QVector<complex> &out);
    void calculateFFT  (const QVector<complex> &in, QVector<double> &out);

    void calculateForwardFFT   (const QVector<complex> &in, QVector<complex> &out);
    void calculateBackwardFFT  (const QVector<complex> &in, QVector<complex> &out);

    void optimize (const QVector<double> &in, const unsigned flags = FFTW_PATIENT);
    void optimize (const QVector<complex> &in, const unsigned flags = FFTW_PATIENT);

    double computePowerSpectrum(const QVector<complex> &in, QVector<double> &out);

private:

    // R and C mean: real and complex
    // CR means: complex to real
    // RC means: real to complex
    // CC means: complex to complex

    double       *mRvec1;               ///< Local copy of incoming real data
    double       *mRvec2;               ///< Local copy of outgoing real data
    fftw_complex *mCvec1;               ///< Local copy of incoming complex data
    fftw_complex *mCvec2;               ///< Local copy of outgoing complex data

    fftw_complex *mCvec3;               ///< Local copy of incoming complex dat
    fftw_complex *mCvec4;               ///< Local copy of outgoing complex data
    fftw_complex *mCvec5;               ///< Local copy of incoming complex dat
    fftw_complex *mCvec6;               ///< Local copy of outgoing complex data

    int       mNRC;                     ///< Size of the FFT real -> complex
    int       mNCR;                     ///< Size of the FFT complex -> real
    int       mNCCF;                    ///< Size of the FFT complex -> complex
    int       mNCCB;                    ///< Size of the FFT complex -> complex

    fftw_plan mPlanRC;                  ///< Plan for FFT real -> complex
    fftw_plan mPlanCR;                  ///< Plan for FFT complex -> real
    fftw_plan mPlanCCF;                 ///< Plan for FFT complex -> complex
    fftw_plan mPlanCCB;                 ///< Plan for FFT complex -> complex
    static QMutex mPlanMutex;           ///< Static mutex protecting planmaking

    void updatePlanCCF (int size, unsigned flags);
    void updatePlanCCB (int size, unsigned flags);
};

#endif // FOURIERTRANSFORM_H
