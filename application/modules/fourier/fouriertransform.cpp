/*****************************************************************************
 * Copyright 2018 Haye Hinrichsen
 *
 * This file is part of Pitchmonitor.
 *
 * Pitchmonitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Pitchmonitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Pitchmonitor. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//             FFTW3 implementation for fast Fourier transformations
//=============================================================================

#include "fouriertransform.h"

#include <QDebug>

#include <cstring>
#include <iostream>
#include <typeinfo>

//-----------------------------------------------------------------------------
//                              static mutex
//-----------------------------------------------------------------------------

QMutex FourierTransform::mPlanMutex;


//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor, clears member variables and checks type consistency
///////////////////////////////////////////////////////////////////////////////

FourierTransform::FourierTransform() :
    mRvec1(nullptr),
    mRvec2(nullptr),
    mCvec1(nullptr),
    mCvec2(nullptr),
    mCvec3(nullptr),
    mCvec4(nullptr),
    mCvec5(nullptr),
    mCvec6(nullptr),
    mNRC(0),
    mNCR(0),
    mNCCF(0),
    mNCCB(0),
    mPlanRC(nullptr),
    mPlanCR(nullptr),
    mPlanCCF(nullptr),
    mPlanCCB(nullptr)
{
}


//-----------------------------------------------------------------------------
//                                Destructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Destructor, deletes plans and local vector copies if existing.
///////////////////////////////////////////////////////////////////////////////

FourierTransform::~FourierTransform()
{
    QMutexLocker lock(&mPlanMutex);
    try
    {
        if (mPlanCCB) fftw_destroy_plan(mPlanCCB);
        if (mPlanCCF) fftw_destroy_plan(mPlanCCF);
        if (mPlanCR) fftw_destroy_plan(mPlanCR);
        if (mPlanRC) fftw_destroy_plan(mPlanRC);
        if (mCvec6) fftw_free(mCvec6);
        if (mCvec5) fftw_free(mCvec5);
        if (mCvec4) fftw_free(mCvec4);
        if (mCvec3) fftw_free(mCvec3);
        if (mCvec2) fftw_free(mCvec2);
        if (mCvec1) fftw_free(mCvec1);
        if (mRvec2) free(mRvec2);
        if (mRvec1) free(mRvec1);
    }
    catch (...)
    { qCritical() << "ERROR: fftw3_destroy_plan throwed an exception"; }
}



//-----------------------------------------------------------------------------
//          Construct a plan for transformations real->complex
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Construct a new plan for the FFT transformation if necessary.
///
/// This function checks whether the plan and the local vectors still
/// exist and whether they have the correct size. If so, the function does
/// nothing which means that the existing plan will be reused. Ohterwise,
/// it deletes the plan and the vectors and constructs them again
/// with the given size.
///
/// \param in : vector of real numbers of size N
/// \param flags : fftw3 internal flags controlling planmaking
///////////////////////////////////////////////////////////////////////////////

void FourierTransform::optimize (const QVector<double> &in,
                                 const unsigned flags)
{
    // if plan still exists and input vector has the same size do nothing
    if (mPlanRC and mRvec1 and mCvec2 and in.size()==mNRC) return;
    if (in.empty())
        { qWarning() << "Fouriertransform::updatePlan called with empty vector"; return; }

    QMutexLocker lock(&mPlanMutex);
    try {
        // delete old plan and vectors
        if (mPlanRC) fftw_destroy_plan(mPlanRC);
        if (mRvec1) free(mRvec1);
        if (mCvec2) fftw_free(mCvec2);

        // allocate new vectors and create a new plan
        mNRC   = in.size();
        mRvec1 = (double *) malloc(mNRC*sizeof(double));
        mCvec2 = (fftw_complex*) fftw_malloc((mNRC/2+1)*sizeof(fftw_complex));
        if (mRvec1==nullptr or mCvec2==nullptr) throw;
        mPlanRC = fftw_plan_dft_r2c_1d (mNRC, mRvec1, mCvec2, flags);
    }
    catch (...) { qCritical() << "FourierTransform::updatePlan: fftw_pplan_dft_r2c_1d throwed an exception"; }
}

//-----------------------------------------------------------------------------
//           Construct a plan for transformations complex->real
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Construct a new plan for the FFT transformation if necessary.
///
/// This function checks whether the plan and the local vectors still
/// exist and whether they have the correct size. If so, the function does
/// nothing which means that the existing plan will be reused. Ohterwise,
/// it deletes the plan and the vectors and constructs them again
/// with the given size.
///
/// \param in : vector of complex numbers
/// \param flags : fftw3 internal flags controlling planmaking
///////////////////////////////////////////////////////////////////////////////

void FourierTransform::optimize (const QVector<complex> &in,
                                 const unsigned flags)
{
    // if plan still exists and input vector has the same size do nothing
    if (mPlanCR and mCvec1 and mRvec2 and in.size()==mNCR/2+1) return;
    if (in.empty())
        { qWarning() << "Fouriertransform::updatePlan called with empty vector"; return; }

    QMutexLocker lock(&mPlanMutex);
    try {
        // delete old plan and vectors
        if (mPlanCR) fftw_destroy_plan(mPlanCR);
        if (mCvec1) fftw_free(mCvec1);
        if (mRvec2) free(mRvec2);

        // allocate new vectors and create a new plan
        mNCR   = 2*in.size()-2;
        mCvec1 = (fftw_complex*) fftw_malloc((mNCR/2+1)*sizeof(fftw_complex));
        mRvec2 = (double *) malloc(mNCR*sizeof(double));
        if (not mCvec1 or not mRvec2) throw;
        mPlanCR = fftw_plan_dft_c2r_1d (mNCR, mCvec1, mRvec2, flags);
    }
    catch (...) { qCritical() << "FourierTransform::updatePlan: fftw_pplan_dft_r2c_1d throwed an exception"; }
}


//-----------------------------------------------------------------------------
//                  Compute non-normalized power spectrum
//-----------------------------------------------------------------------------

double FourierTransform::computePowerSpectrum(const QVector<FourierTransform::complex> &in, QVector<double> &out)
{
    if (out.size() != in.size()) out.resize(in.size());
    double norm = 0;
    for (int i=0; i<in.size(); i++)
    {
        double entry = in[i].real()*in[i].real() + in[i].imag()*in[i].imag();
        norm += entry;
        out[i] = entry;
    }
    return norm;
}


//-----------------------------------------------------------------------------
//   Private function: construct a plan for transformations complex->complex
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Construct a new plan for the FFT transformation if necessary.
///
/// This function checks whether the plan and the local vectors still
/// exist and whether they have the correct size. If so, the function does
/// nothing which means that the existing plan will be reused. Ohterwise,
/// it deletes the plan and the vectors and constructs them again
/// with the given size.
///
/// \param size : size of the vectors
/// \param flags : fftw3 internal flags controlling planmaking
///////////////////////////////////////////////////////////////////////////////

void FourierTransform::updatePlanCCF (int size, unsigned flags)
{
    // if plan still exists and input vector has the same size do nothing
    if (mPlanCCF and mCvec3 and mCvec4 and size==mNCCF) return;
    if (size==0)
        { qWarning() << "Fouriertransform::updatePlan called with empty vector"; return; }

    QMutexLocker lock(&mPlanMutex);
    try {
        // delete old plan and vectors
        if (mPlanCCF) fftw_destroy_plan(mPlanCCF);
        if (mCvec3) fftw_free(mCvec3);
        if (mCvec4) fftw_free(mCvec4);

        // allocate new vectors and create a new plan
        mNCCF   = size;
        mCvec3 = (fftw_complex*) fftw_malloc((mNCCF)*sizeof(fftw_complex));
        mCvec4 = (fftw_complex*) fftw_malloc((mNCCF)*sizeof(fftw_complex));
        if (not mCvec3 or not mCvec4) throw;
        mPlanCCF = fftw_plan_dft_1d (mNCCF, mCvec3, mCvec4, FFTW_FORWARD, flags);
    }
    catch (...) { qCritical() << "FourierTransform::updatePlan: fftw_plan_dft_1d throwed an exception"; }
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Construct a new plan for the FFT transformation if necessary.
///
/// This function checks whether the plan and the local vectors still
/// exist and whether they have the correct size. If so, the function does
/// nothing which means that the existing plan will be reused. Ohterwise,
/// it deletes the plan and the vectors and constructs them again
/// with the given size.
///
/// \param size : size of the vectors
/// \param flags : fftw3 internal flags controlling planmaking
///////////////////////////////////////////////////////////////////////////////
///
void FourierTransform::updatePlanCCB (int size, unsigned flags)
{
    // if plan still exists and input vector has the same size do nothing
    if (mPlanCCB and mCvec5 and mCvec6 and size==mNCCB) return;
    if (size==0)
        { qWarning() << "Fouriertransform::updatePlan called with empty vector"; return; }

    QMutexLocker lock(&mPlanMutex);
    try {
        // delete old plan and vectors
        if (mPlanCCB) fftw_destroy_plan(mPlanCCB);
        if (mCvec5) fftw_free(mCvec5);
        if (mCvec6) fftw_free(mCvec6);

        // allocate new vectors and create a new plan
        mNCCB   = size;
        mCvec5 = (fftw_complex*) fftw_malloc((mNCCB)*sizeof(fftw_complex));
        mCvec6 = (fftw_complex*) fftw_malloc((mNCCB)*sizeof(fftw_complex));
        if (not mCvec5 or not mCvec6) throw;
        mPlanCCB = fftw_plan_dft_1d (mNCCB, mCvec5, mCvec6, FFTW_BACKWARD, flags);
    }
    catch (...) { qCritical() << "FourierTransform::updatePlan: fftw_plan_dft_1d throwed an exception"; }
}


//-----------------------------------------------------------------------------
//                   Calculate forward FFT real -> complex
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Foward FFT, mapping a real vector with size N to a complex one with
/// size N/2+1.
///
/// \param in : vector of real numbers of size N
/// \param out : vector of complex numbers, will be resized to N/2+1
///////////////////////////////////////////////////////////////////////////////

void FourierTransform::calculateFFT  (const QVector<double> &in, QVector<complex> &out)
{
    if (in.empty()) return;
    if (out.size() != in.size()/2+1) out.resize(in.size()/2+1);
    optimize(in,FFTW_ESTIMATE); // only executed if there is no better optimization
    if (in.size()!=mNRC or out.size()!=mNRC/2+1)
    { qCritical() << "calculateFFT: Vector lengths inconsistent."; return; }
    try {
        std::memcpy(mRvec1,in.data(),mNRC*sizeof(double));
        fftw_execute(mPlanRC);
        std::memcpy(out.data(),mCvec2,(mNRC/2+1)*sizeof(fftw_complex));
    }
    catch (...) { qCritical() << "FourierTransform::calculateFFT: fftw_execute throwed an exception"; }
}


//-----------------------------------------------------------------------------
//                   Calculate backward FFT complex -> real
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Backward FFT, mapping a complex vector with size M to a
/// real one with size 2M-1.
///
/// \param in : vector of complex numbers of size M
/// \param out : vector of real numbers, will be resized to size 2*M-1
///////////////////////////////////////////////////////////////////////////////

void FourierTransform::calculateFFT  (const QVector<complex> &in, QVector<double> &out)
{
    if (in.empty()) return;
    if (out.size() != 2*in.size()-2) out.resize(2*in.size()-2);
    optimize(in,FFTW_ESTIMATE); // only executed if there is no better optimization
    if (in.size()!=mNCR/2+1 or out.size()!=mNCR)
    { qCritical() << "calculateFFT: Vector lengths inconsistent."; return; }
    try {
        std::memcpy(mCvec1,in.data(),(mNCR/2+1)*sizeof(fftw_complex));
        fftw_execute(mPlanCR);
        std::memcpy(out.data(),mRvec2,mNCR*sizeof(double));
    }
    catch (...) { qCritical() << "FourierTransform::calculateFFT: fftw_execute throwed an exception"; }
}


//-----------------------------------------------------------------------------
//                   Calculate forward FFT complex -> complex
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Forward FFT, mapping a complex vector with size M to a
/// complex one with size M.
///
/// \param in : vector of complex numbers of size M
/// \param out : vector of real numbers, will be resized to size 2*M-1
///////////////////////////////////////////////////////////////////////////////

void FourierTransform::calculateForwardFFT  (const QVector<complex> &in, QVector<complex> &out)
{
    if (in.empty()) return;
    if (out.size() != in.size()) out.resize(in.size());
    updatePlanCCF(in.size(),FFTW_ESTIMATE);
    if (in.size()!=mNCCF or out.size()!=mNCCF)
    { qCritical() << "calculateForwardFFT: Vector lengths inconsistent."; return; }
    try {
        std::memcpy(mCvec3,in.data(),mNCCF*sizeof(fftw_complex));
        fftw_execute(mPlanCCF);
        std::memcpy(out.data(),mCvec4,mNCCF*sizeof(fftw_complex));
    }
    catch (...) { qCritical() << "FourierTransform::calculateFFT: fftw_execute throwed an exception"; }
}


//-----------------------------------------------------------------------------
//                   Calculate backward FFT complex -> complex
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Backward FFT, mapping a complex vector with size M to a
/// complex one with size M.
///
/// \param in : vector of complex numbers of size M
/// \param out : vector of real numbers, will be resized to size 2*M-1
///////////////////////////////////////////////////////////////////////////////

void FourierTransform::calculateBackwardFFT  (const QVector<complex> &in, QVector<complex> &out)
{
    if (in.empty()) return;
    if (out.size() != in.size()) out.resize(in.size());
    updatePlanCCB(in.size(),FFTW_ESTIMATE);
    if (in.size()!=mNCCB or out.size()!=mNCCB)
    { qCritical() << "calculateBackwardFFT: Vector lengths inconsistent."; return; }
    try {
        std::memcpy(mCvec5,in.data(),mNCCB*sizeof(fftw_complex));
        fftw_execute(mPlanCCB);
        std::memcpy(out.data(),mCvec6,mNCCB*sizeof(fftw_complex));
    }
    catch (...) { qCritical() << "FourierTransform::calculateFFT: fftw_execute throwed an exception"; }
}
