
# additional include paths
android {
    INCLUDEPATH += $$PWD/config/android
}
else:linux {
    INCLUDEPATH += $$PWD/config/linux
}
else:ios {
    INCLUDEPATH += $$PWD/config/ios
}
else:macx {
    INCLUDEPATH += $$PWD/config/osx
}
else:win32 {
    INCLUDEPATH += $$PWD/config/windows
}


