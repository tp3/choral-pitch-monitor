#-------------------------------------------------
#
# Project created by QtCreator 2018-12-14T23:23:07
#
#-------------------------------------------------

QT       -= core gui

TARGET = rdftscalar
TEMPLATE = lib
CONFIG += staticlib

DEFINES += RDFT_LIBRARY

DEFINES += QT_DEPRECATED_WARNINGS

include(../../config.pri)

LIBPATH=$$PWD/../../fftw3/rdft/scalar
INCLUDEPATH += $$PWD/../../fftw3

SOURCES += $$files($$LIBPATH/*.c,false)
HEADERS += $$files($$LIBPATH/*.h,false)

# unix {
#     target.path = /usr/lib
#     INSTALLS += target
# }
