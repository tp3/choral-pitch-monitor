#-------------------------------------------------
#
# Project created by QtCreator 2018-12-14T23:23:07
#
#-------------------------------------------------

QT       -= core gui

TARGET = rdftscalarr2cf
TEMPLATE = lib
CONFIG += staticlib

DEFINES += DFT_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(../../config.pri)

LIBPATH=$$PWD/../../fftw3/rdft/scalar/r2cf
INCLUDEPATH += $$PWD/../../fftw3

SOURCES += $$files($$LIBPATH/*.c,false)
#SOURCES -= $$LIBPATH/simd/common/genus.c
HEADERS += $$files($$LIBPATH/*.h,false)

# unix {
#     target.path = /usr/lib
#     INSTALLS += target
# }
