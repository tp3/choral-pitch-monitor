#-------------------------------------------------
#
# Project created by QtCreator 2018-12-14T23:23:07
#
#-------------------------------------------------

QT       -= core gui

TARGET = kernel
TEMPLATE = lib
CONFIG += staticlib

DEFINES += QT_DEPRECATED_WARNINGS

include(../../config.pri)

LIBPATH=$$PWD/../../fftw3/kernel
INCLUDEPATH += $$PWD/../../fftw3

SOURCES += $$files($$LIBPATH/*.c,true)
HEADERS += $$files($$LIBPATH/*.h,true)
