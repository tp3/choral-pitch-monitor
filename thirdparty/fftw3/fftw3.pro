TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS += \
    api \
    dft \
    dftscalar \
    dftscalarcodelets \
    rdft \
    rdftscalar \
    rdftscalarr2cb \
    rdftscalarr2cf \
    rdftscalarr2r \
    kernel \
    reodft \

# Specify subfolder where to find them
api.subdir = libs/api
dft.subdir = libs/dft
dftscalar.subdir = libs/dftscalar
dftscalarcodelets.subdir = libs/dftscalarcodelets
rdft.subdir = libs/rdft
rdftscalar.subdir = libs/rdftscalar
rdftscalarr2cb.subdir = libs/rdftscalarr2cb
rdftscalarr2cf.subdir = libs/rdftscalarr2cf
rdftscalarr2r.subdir = libs/rdftscalarr2r
kernel.subdir = libs/kernel
reodft.subdir = libs/reodft


# Specify dependencies
dft.depends += dftscalar
dftscalar.depends += dftscalarcodelets
api.depends += dft dftscalar dftscalarcodelets rdft rdftscalar kernel reodft
