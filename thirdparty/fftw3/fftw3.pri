FFTLIBS=$$OUT_PWD/../thirdparty/fftw3/libs

LIBS += -Wl,--start-group
LIBS += -L/$$FFTLIBS/api -lapi
LIBS += -L/$$FFTLIBS/dft -ldft
LIBS += -L/$$FFTLIBS/dftscalar -ldftscalar
LIBS += -L/$$FFTLIBS/dftscalarcodelets -ldftscalarcodelets
LIBS += -L/$$FFTLIBS/rdft -lrdft
LIBS += -L/$$FFTLIBS/rdftscalar -lrdftscalar
LIBS += -L/$$FFTLIBS/rdftscalarr2cb -lrdftscalarr2cb
LIBS += -L/$$FFTLIBS/rdftscalarr2cf -lrdftscalarr2cf
LIBS += -L/$$FFTLIBS/rdftscalarr2r -lrdftscalarr2r
LIBS += -L/$$FFTLIBS/reodft -lreodft
LIBS += -L/$$FFTLIBS/kernel -lkernel
LIBS += -Wl,--end-group

